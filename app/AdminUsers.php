<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Library\Queryhelper;
use DB;

class AdminUsers extends Model {

    protected $table = 'users';

    public function getDataByLoginID($user_name) {
        $select = DB::table($this->table)
            ->where('user_name', '=', $user_name)
            ->select($this->table . '.*');

        $data = $select->get();

        return $data;
    }

    public function searchByParams($params = array()) {
        $select = DB::table($this->table)
            ->select($this->table . '.*');

        $queryHelper = new Queryhelper;

        $select = $queryHelper->bindParams($select, $params);

        return $select->get();
    }

}
