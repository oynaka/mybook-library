<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Library\Queryhelper;
use DB;

class Author extends Model {

    protected $table = 'book_author';
    const CREATED_AT = NULL;
    const UPDATED_AT = NULL;

}
