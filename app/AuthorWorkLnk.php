<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Library\Queryhelper;
use DB;

class AuthorWorkLnk extends Model {

    protected $table = 'authorworklnk';
    const CREATED_AT = NULL;
    const UPDATED_AT = NULL;

    public function authors() {
        return $this->belongsTo('App\Author','author_id');
    }

    public function updateMangaAuthor($mangaID,$authorArr) {

        DB::table($this->table)->where('type',1)
                               ->where('book_id', '=', $mangaID)
                               ->delete();

        DB::statement('ALTER TABLE authorworklnk AUTO_INCREMENT=0;');

        if(count($authorArr) > 0)
        {
            DB::table($this->table)->insert($authorArr);
        }

    }

    public function updateNovelAuthor($novelID,$authorID) {
        DB::table($this->table)->where('type',2)
                               ->where('role_id',1)
                               ->where('book_id',$novelID)
                               ->update(['author_id' => $authorID]);
        return true;
    }

    public function updateBookAuthor($book_id,$author_id,$type,$role_id) {
        DB::table($this->table)->where('type',$type)
                               ->where('role_id',$role_id)
                               ->where('book_id',$book_id)
                               ->update(['author_id' => $author_id]);
        return true;
    }    
}
