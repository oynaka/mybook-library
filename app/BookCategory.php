<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Library\Queryhelper;
use DB;

class BookCategory extends Model {

    protected $table = 'book_categories';
    const CREATED_AT = NULL;
    const UPDATED_AT = NULL;

    static function resetAutoIncrement() {
        DB::statement('ALTER TABLE book_categories AUTO_INCREMENT=0;');
    }

    public function manga() {
        return $this->belongsTo('App\Manga','bookID');
    }    

    public function category() {
        return $this->belongsTo('App\Category','catID');
    }    

}
