<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Library\Queryhelper;
use DB;

class BookLocation extends Model {

    protected $table = 'book_location';
    const CREATED_AT = NULL;
    const UPDATED_AT = NULL;

    public function mangaDetail() {
        return $this->hasMany('App\MangaDetail','mangaLocation','id');
    }

    public function novelDetail() {
        return $this->hasMany('App\NovelDetail','novelLocation','id');
    }

}
