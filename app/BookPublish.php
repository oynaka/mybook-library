<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Library\Queryhelper;
use DB;

class BookPublish extends Model {

    protected $table = 'book_publish';
    const CREATED_AT = NULL;
    const UPDATED_AT = NULL;

    public function manga() {
        return $this->hasMany('App\Manga','publishID','id');
    }

    function mangaDetail()
    {
        return $this->hasManyThrough('App\MangaDetail','App\Manga','publishID','mangaID','id','mangaID')
                    ->whereRaw("manga.mangastatus != 'S'");
    }

}
