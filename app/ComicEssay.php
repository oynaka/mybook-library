<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Library\Queryhelper;
use DB;

class ComicEssay extends Model {

    protected $table = 'comic_essay';
    protected $primaryKey = 'ceID';
    const CREATED_AT = 'createdTime';
    const UPDATED_AT = 'updatedTime';

    static $bookStatus = [ ['id' => 'C','name' => 'Complete'] ,
                            ['id' => 'O','name' => 'Ongoing'] ,
                            ['id' => 'H','name' => 'Hitius'] ,
                            ['id' => 'S','name' => 'Sold'] ];
    
    static function getStatus($status) {
        $output = '';
        switch($status) {
            case 'C' : $output = 'Complete';break;
            case 'O' : $output = 'Ongoing';break;
            case 'H' : $output = 'Hitius';break;
            case 'S' : $output = 'Sold';break;
        }
        return $output;
    }    

    public function queryAllComicEssay($search_name=null,$author_id=null) {

        $search_author[] = ['authorworklnk.type','=',3];
        if(!empty($author_id)) {
            $search_author[] = ['authorworklnk.author_id','=',$author_id];
        }

        $output = DB::table($this->table)
                 ->leftJoin('authorworklnk',$this->table.'.ceID','authorworklnk.book_id')
                 ->whereRaw('(ceName like "%'.$search_name.'%" or ceNameEng like "%'.$search_name.'%")')
                 ->whereIn('role_id',[1, 2, 3])
                 ->where($search_author)
                 ->selectRaw('distinct '.$this->table.'.*')
                 ->get();

        return $output;

    }    

    public function insertCE($data) {

        $ceID = DB::transaction(function() use ($data) {
            //echo '<pre>';print_r($data);echo '</pre>';
            
            $ceID = DB::table($this->table)->insertGetId(
                            [
                              'ceCode' => $data['ceCode'],
                              'ceName' => $data['ceName'],
                              'ceNameEng' => $data['ceNameEng'],
                              'native_name' => $data['native_name'],
                              'publishID' => $data['publishID'],
                              'status' => $data['status']
                            ]
            );
            
            //insert novel detail
            DB::table('comic_essay_detail')->insert(
                [
                  'ceID' => $ceID,
                  'vol' => $data['vol'],
                  'quan' => 1,
                  'price' => $data['price'],
                  'date_buy' => $data['date_buy']
                ]
            );
            
            //insert author
            DB::table('authorworklnk')->insert(
                [
                  'book_id' => $ceID,
                  'type' => 3,
                  'role_id' => 1,
                  'author_id' => $data['author_id']
                ]
            );

            return $ceID;
        });

        return $ceID;
    }  
    
    public function countCEDetail($whereArr=null) {
        $select = DB::table($this->table)
                        ->leftJoin('comic_essay_detail',$this->table.'.ceID' , '=','comic_essay_detail.ceID');

        if(!empty($whereArr)) {
            $select->where($whereArr);
        }

        $output = $select->count();

        return $output;
    }    

}
