<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Library\Queryhelper;
use DB;

class ComicEssayDetail extends Model {

    protected $table = 'comic_essay_detail';
    protected $primaryKey = 'ce_detail_id';

    const CREATED_AT = 'createdTime';
    const UPDATED_AT = 'updatedTime';

    public function ce() {
        return $this->belongsTo('App\ComicEssay','ceID');
    }
}
