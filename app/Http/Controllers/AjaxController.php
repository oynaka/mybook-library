<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Config;
use Session;
use View;
use Route;
use App\Author;
use App\Manga;
use App\MangaDetail;
use App\NovelDetail;
use App\ComicEssayDetail;
use App\BookPublish;
use App\AuthorWorkLnk;

class AjaxController extends AuthenController {
    
    private $data = array();

    public function __construct(Request $request) {
        parent::__construct();
    }

    
    public function init()
    {
        
    }
    
    public function callAuthor(Request $request) {
        $params = $request->all();
        $author_data = Author::where('name','like','%'.$params['q'].'%')
                        ->selectRaw('id,name as authorName')
                        ->orderBy('name')
                        ->get();

        //echo '<pre>';print_r($author_data);echo '</pre>';
        //exit();
        header("Content-Type: application/json; charset=utf-8");
        echo json_encode($author_data);
    }

    public function getMangaDetail(Request $request) {
        $bookDetail = MangaDetail::find($request->mangaDetailID);
        header("Content-Type: application/json; charset=utf-8");
        echo json_encode($bookDetail);
    }

    public function getNovelDetail(Request $request) {
        $bookDetail = NovelDetail::find($request->novelDetailID);
        header("Content-Type: application/json; charset=utf-8");
        echo json_encode($bookDetail);
    }

    public function getCEDetail(Request $request) {
        $bookDetail = ComicEssayDetail::find($request->ceDetailID);
        header("Content-Type: application/json; charset=utf-8");
        echo json_encode($bookDetail);
    }    

    public function setAreaGraph($year=null) {
        if(is_null($year)) {
            $year = date('Y');
        }

        $data=null;
        for($i=1;$i<=12;$i++) {
            $temp = null;
            $temp['period'] = $year.'-'.sprintf("%02d",$i);
            $temp['Manga'] = MangaDetail::where('mangaDatebuy','like','%'.$temp['period'].'%')->count();
            $temp['Novel'] = NovelDetail::where('novelDatebuy','like','%'.$temp['period'].'%')->count();
            $temp['comicessay'] = ComicEssayDetail::where('date_buy','like','%'.$temp['period'].'%')->count();
            
            $data[] = $temp;
        }
        
        //echo '<pre>';print_r($data);echo '</pre>';
        echo json_encode($data);
    }

    public function setBarGraph() {
        $startYear = '2008';
        $endYear = date('Y');

        for($i=$startYear;$i<=$endYear;$i++) {
            $betweenArr = [$i."-01-01",$i."-12-31"];
            $temp = null;
            $temp['y'] = $i;
            $temp['Manga'] = MangaDetail::whereBetween('mangaDatebuy',$betweenArr)->count();
            $temp['Novel'] = NovelDetail::whereBetween('novelDatebuy',$betweenArr)->count();
            $temp['comicessay'] = ComicEssayDetail::whereBetween('date_buy',$betweenArr)->count();

            $data[] = $temp;
        }
        //echo '<pre>';print_r($data);echo '</pre>';
        echo json_encode($data);
    }

    public function setBarReadGraph() {
        $startYear = 2021;
        $endYear = date('Y');

        for($i=$startYear;$i<=$endYear;$i++) {
            $betweenArr = [$i."-01-01",$i."-12-31"];
            $temp = null;
            $temp['year'] = strval($i);
            $temp['Manga'] = MangaDetail::whereBetween('readFlgDate',$betweenArr)->count();
            $temp['Novel'] = NovelDetail::whereBetween('readFlgDate',$betweenArr)->count();
            $temp['value'] = $temp['Manga'] + $temp['Novel'];

            $data[] = $temp;
        }
        //echo '<pre>';print_r($data);echo '</pre>';
        echo json_encode($data);
    }

    public function setDonutGraph($year=null) {
        if(is_null($year)) {
            $year = date('Y');
        }

        $allPublish = BookPublish::all();

        $obj = new Manga;
        $arr = $obj->getPublishMangaByYear($year);

        if(count($arr) > 0) {
            foreach($arr as $val) {
                $temp = null;
                $temp['label'] = $allPublish->find($val->publishID)->name;
                $temp['value'] = $val->num;
                $data[] = $temp;
            }
        }

        //echo '<pre>';print_r($data);echo '</pre>';
        echo json_encode($data);
    }

    public function getMangaAuthor($mangaID) {
        if($mangaID == '') {
            return null;
        }
        $data = AuthorWorkLnk::with('authors')
                             ->where('type',1)
                             ->where('book_id',$mangaID)
                             ->orderby('role_id')
                             ->orderby('author_id')
                             ->select('book_id','author_id','role_id')
                             ->get();
        header("Content-Type: application/json; charset=utf-8");
        echo json_encode($data);
    }

    public function getMangaRemark(Request $request) {
        $manga = Manga::find($request->manga_id,['remark']);
        header("Content-Type: application/json; charset=utf-8");
        echo json_encode($manga);
    }
}
