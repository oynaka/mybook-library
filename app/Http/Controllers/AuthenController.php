<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Session;
use Redirect;
use Config;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\AdminUsers;

class AuthenController extends Controller {

    public function __construct() {
        $this->middleware(function ($request, $next) {

            $user_id = Session::get('user_id');

            if ($user_id == NULL) {
                return Redirect::route('login')->send();
            }

            $usersModel = new AdminUsers;

            $databaseParams = array();

            $databaseParams['user_id'] = $user_id;

            $loginUsers = $usersModel->searchByParams($databaseParams);
            
            $loginProfile = array();
            foreach ($loginUsers as $key=>$login) {
                $loginProfile[$key]['user_id'] = $login->user_id;
                $loginProfile[$key]['user_name'] = $login->user_name;
            }
            
            Config::set('loginProfile', reset($loginProfile));

            return $next($request);
        });
    }

}
