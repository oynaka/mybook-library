<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Config;
use Session;
use View;
use Route;
use App\Manga;
use App\Novel;
use App\ComicEssay;
use App\Author;
use App\AuthorWorkLnk;

class AuthorController extends AuthenController {

    private $data = array();

    public function __construct(Request $request) {
        parent::__construct();

    }

    public function init(Request $request)
    {
        $mangaModel = new Manga();
        $novelModel =  new Novel();

        $params = $request->all();

        $search_name = '';
        if(isset($params['search_name'])) {
            $search_name = $params['search_name'];
        }

        //echo '<pre>';print_r($params);echo '</pre>';
        //echo '<pre>';print_r(session()->all());echo '</pre>';

        $whereArr = null;
        $whereArr = [['name', 'like', '%'.$search_name.'%']];
        $allAuthor = Author::where($whereArr)->paginate(10);


        //example work
        foreach($allAuthor as $key => $val) {

            $sampleWork = AuthorWorkLnk::where('author_id',$val->id)
                                        ->orderBy('type','asc')
                                        ->orderBy('role_id','asc')
                                        ->orderBy('book_id','asc')
                                        ->limit(1)
                                        ->first();

            //echo '<pre>';print_r($sampleWork->toArray());echo '</pre><hr>';
            if(isset($sampleWork)) {
                $allAuthor[$key]->sampleBookID = $sampleWork->book_id;
                if($sampleWork->type == 1) {
                    $allAuthor[$key]->sampleBookType = 'manga';
                    $allAuthor[$key]->sampleBookName = Manga::find($sampleWork->book_id)->mangaName;
                } else if($sampleWork->type == 2) {
                    $allAuthor[$key]->sampleBookType = 'novel';
                    $allAuthor[$key]->sampleBookName = Novel::find($sampleWork->book_id)->novelName;
                } else if($sampleWork->type == 3)  {
                    $allAuthor[$key]->sampleBookType = 'comicessay';
                    $allAuthor[$key]->sampleBookName = ComicEssay::find($sampleWork->book_id)->ceName;
                }
            }
        }

        //echo '<pre>';print_r($allAuthor->toArray());echo '</pre>';

        $this->data = null;
        $this->data['allAuthor'] = $allAuthor;
        $this->data['search_name'] = $search_name;

        return view('author.index',$this->data);
    }


    public function edit(Request $request,$id) {
        $params = $request->all();

        if(Session::get('flash_message') == 'updated') {
            Session::forget('flash_message');
        }

        //echo '<pre>';print_r($author);echo '</pre>';
        //Book::find($id)->update([ 'author_id' => 10]);

        $author = Author::find($id);

        if(isset($params['mode'])) {
            if($params['mode'] == 'update') {

                $validatedData = $request->validate([
                    'name' => 'required',
                ], $this->errorMessages() );

                $author->name = $request->name;
                $author->native_name = $request->native_name;
                $author->save();

                Session::flash('flash_message', 'updated');
            }
        }

        $this->data = null;
        $this->data['author'] = $author;
        return view('author.edit',$this->data);
    }



    public function insert(Request $request) {
        $params = $request->all();

        $objModel = new Author;

        if(isset($params['mode'])) {
            if($params['mode'] == 'insert') {

                $validatedData = $request->validate([
                    'name' => 'required',
                ], $this->errorMessages() );

                $objModel->name = $request->name;
                $objModel->native_name = $request->native_name;
                $objModel->save();
                Session::flash('flash_message', 'inserted');
                return redirect()->route('author_edit', $objModel->id);
            }
        }

        return view('author.insert');
    }


    public function errorMessages()
    {
        return [
            'name.required' => 'Author Name is required',
        ];
    }
}
