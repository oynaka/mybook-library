<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Config;
use Session;
use View;
use Route;
use App\BookLocation;
use App\MangaDetail;
use App\NovelDetail;
use App\BookOriPublish;

class BookLocationController extends AuthenController {
    
    private $data = array();

    public function __construct(Request $request) {
        parent::__construct();
        
    }
    
    public function init(Request $request)
    {
        $params = $request->all();

        $search_name = '';
        if(isset($params['search_name'])) {
            $search_name = $params['search_name'];
        }

        //echo '<pre>';print_r($params);echo '</pre>';
        //echo '<pre>';print_r(session()->all());echo '</pre>';

        $whereArr = null;
        $whereArr = [['location', 'like', '%'.$search_name.'%']];
        //$allBookLocation = Booklocation::paginate(10);
        $allBookLocation = Booklocation::withCount('mangaDetail','novelDetail')
                           ->where($whereArr)
                           ->paginate(10);

        //echo '<pre>';print_r($allBookLocation);echo '</pre>';

        $this->data = null;
        $this->data['book_location_list'] = $allBookLocation;
        $this->data['search_name'] = $search_name;

        return view('booklocation.index',$this->data);
    }

    public function view($location_id) {

        $locationInfo = Booklocation::find($location_id); 
        
        $mangaInfo = MangaDetail::with('manga')
                        ->where('mangaLocation',$location_id)
                        ->orderBy('mangaID','asc')
                        ->orderBy('mangaVol','asc')
                        ->get();

        $allOriPublish = BookOriPublish::all();

        foreach($mangaInfo as $obj) {
            $obj->oriPublish = '';
            $tmpName = $allOriPublish->where('id',$obj->manga->oriPublishID)->first();
            if($tmpName != '') {
                $obj->oriPublish = $tmpName->name;
            }
        }

        //echo '<pre>';print_r($mangaInfo[0]);echo '</pre>';

        $novelInfo = NovelDetail::with('novel')
                        ->where('novelLocation',$location_id)
                        ->orderBy('novelID','asc')
                        ->orderBy('novelVol','asc')
                        ->get();

        $this->data = null;
        $this->data['name'] = $locationInfo->location;
        $this->data['mangaList'] = $mangaInfo;
        $this->data['totalManga'] = $mangaInfo->count();
        $this->data['novelList'] = $novelInfo;
        $this->data['totalNovel'] = $novelInfo->count();


        return view('booklocation.view',$this->data);
    }
    
    public function edit(Request $request,$location_id) {
        $params = $request->all();
        //Session::forget('flash_message');

        //echo '<pre>';print_r($params);echo '</pre>';
        //Book::find($id)->update([ 'author_id' => 10]);

        $bookLocationModel = Booklocation::find($location_id);

        if(isset($params['mode'])) {
            if($params['mode'] == 'update') {

                $validatedData = $request->validate([
                    'location' => 'required',
                ], $this->errorMessages() );

                $bookLocationModel->location = $request->location;
                $bookLocationModel->save();

                Session::flash('flash_message', 'updated');
            }
        } 

        $this->data = null;
        $this->data['locationInfo'] = $bookLocationModel;
        return view('booklocation.edit',$this->data);
    }
    
    public function insert(Request $request) {

        $params = $request->all();

        $bookLocationModel = new Booklocation;

        if(isset($params['mode'])) {
            if($params['mode'] == 'insert') { 

                $validatedData = $request->validate([
                    'location' => 'required',
                ], $this->errorMessages() );

                $bookLocationModel->location = $request->location;
                $bookLocationModel->save();
                Session::flash('flash_message', 'inserted');
                return redirect()->route('booklocation_edit', $bookLocationModel->id);
            }
        }

        return view('booklocation.insert');
    }

    public function errorMessages()
    {
        return [
            'location.required' => 'Book Location is required',
        ];
    }    

    /*
สมมติเพนมี
$shops = DB::table('shops')
           ->leftJoin('users', 'shops.account_id', '=', 'users.id')
           ->leftJoin('shop_types', 'shops.shop_type', '=', 'shop_types.id')
           ->leftJoin('shop_categories', 'shops.shop_category', '=', 'shop_categories.id')
           ->leftJoin('floor_zone_mappings', 'shops.zone_mapping_id', '=', 'floor_zone_mappings.id')
           ->leftJoin('floors', 'floor_zone_mappings.floor_id', '=', 'floors.id')
           ->leftJoin('zones', 'floor_zone_mappings.zone_id', '=', 'zones.id')
           ->select('shops.*',
               'users.id as account_id',
               'users.name as account_name',
               'shop_types.name as type_name',
               'shop_categories.name as category_name',
               'zones.name as zone_name',
               'floors.name as floor_name'
           )
           ->get();

$limit = intval(Input::get('limit', 10));
$page = intval(Input::get('page', 1));
$paginator = new LengthAwarePaginator($shops->forPage($page, $limit), $shops->count(), $limit, $page, []);
$dataArray = $paginator->items();
เป็นการสร้าง pagination แบบ custom เอง
โดยใช้  use Illuminate\Pagination\LengthAwarePaginator;
ตอนที่จะ return ไปให้ front ก็ใช้แบบนี้ก็ได้
[
               'rows' => $dataArray,
               'pagination' => [
                   'total_items' => $paginator->total(),
                   'current_page_items' => count($dataArray),
                   'items_per_page' => $paginator->perPage(),
                   'current_page' => $paginator->currentPage(),
                   'total_pages' => $paginator->lastPage(),
               ]
ได้ผลเหมือนกันกับ ::paginate(10)

    */



}
