<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Config;
use Session;
use View;
use Route;
use App\Manga;
use App\MangaDetail;
use App\Novel;
use App\BookOriPublish;

class BookOriPublishController extends AuthenController {
    
    private $data = array();

    public function __construct(Request $request) {
        parent::__construct();
        
    }
    
    public function init(Request $request)
    {
        $mangaModel = new Manga();
        $novelModel =  new Novel();

        $params = $request->all();

        $search_name = '';
        if(isset($params['search_name'])) {
            $search_name = $params['search_name'];
        }

        //echo '<pre>';print_r($params);echo '</pre>';
        //echo '<pre>';print_r(session()->all());echo '</pre>';

        $whereArr = null;
        $whereArr = [['name', 'like', '%'.$search_name.'%']];
        $allArr = BookOriPublish::where($whereArr)->paginate(10);

        foreach($allArr as $key => $val) {
            $whereArr = [['oriPublishID','=',$val->id]];
            $allArr[$key]->mangaSeriesCount = $mangaModel->countManga($whereArr);
            $allArr[$key]->mangaBooksCount = $mangaModel->countMangaDetail($whereArr);
        }

        $this->data = null;
        $this->data['allArr'] = $allArr;
        $this->data['search_name'] = $search_name;

        return view('bookoripublish.index',$this->data);
    }

    public function view($id) {

        $novelModel = new Novel();

        $publishInfo = BookOriPublish::find($id);
        $mangaList = Manga::with('publish')->where('oriPublishID',$id)->get();

        foreach ($mangaList as $key => $manga) {

            $isCloud = MangaDetail::where('mangaID',$manga->mangaID)
                        ->where('mangaLocation',9)
                        ->count();

            if($isCloud > 0) {
                //echo $manga->mangaName.'<br>';
                $mangaList[$key]->isCloud = 1;
            } else {
                $mangaList[$key]->isCloud = 0;
            }
        }
        

        $this->data = null;
        $this->data['name'] = $publishInfo->name;
        $this->data['mangaList'] = $mangaList;

        $this->data['novelList'] = null;
        $this->data['totalNovel'] = 0;

        return view('bookoripublish.view',$this->data);
    }
    
    public function edit(Request $request,$id) {
        $params = $request->all();
        //Session::forget('flash_message');

        //echo '<pre>';print_r($params);echo '</pre>';
        //Book::find($id)->update([ 'author_id' => 10]);

        $modelOjb = BookOriPublish::find($id);

        if(isset($params['mode'])) {
            if($params['mode'] == 'update') {

                $validatedData = $request->validate([
                    'name' => 'required',
                ], $this->errorMessages() );

                $modelOjb->name = $request->name;
                $modelOjb->name_jp = $request->name_jp;
                $modelOjb->save();

                Session::flash('flash_message', 'updated');
            }
        } 

        $this->data = null;
        $this->data['arrInfo'] = $modelOjb;
        return view('bookoripublish.edit',$this->data);
    }
    
    public function insert(Request $request) {

        $params = $request->all();

        $modelObj = new BookOriPublish;

        if(isset($params['mode'])) {
            if($params['mode'] == 'insert') { 

                $validatedData = $request->validate([
                    'name' => 'required',
                ], $this->errorMessages() );

                $modelObj->name = $request->name;
                $modelObj->name_jp = $request->name_jp;
                $modelObj->save();
                Session::flash('flash_message', 'inserted');
                return redirect()->route('oripublish_edit', $modelObj->id);
            }
        }

        return view('bookoripublish.insert');
    }

    public function errorMessages()
    {
        return [
            'name.required' => 'Publish name is required',
        ];
    }    

}
