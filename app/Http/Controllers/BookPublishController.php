<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Config;
use Session;
use View;
use Route;
use App\Manga;
use App\Novel;
use App\ComicEssay;
use App\BookPublish;

class BookPublishController extends AuthenController {
    
    private $data = array();

    public function __construct(Request $request) {
        parent::__construct();
        
    }
    
    public function init(Request $request)
    {
        //$temp = Manga::with('mangaDetail')->where([['publishID','=',2]])->get();

        $novelModel =  new Novel();
        
        $params = $request->all();

        $search_name = '';
        if(isset($params['search_name'])) {
            $search_name = $params['search_name'];
        }

        //echo '<pre>';print_r($params);echo '</pre>';
        //echo '<pre>';print_r(session()->all());echo '</pre>';

        $whereArr = null;
        $whereArr = [['name', 'like', '%'.$search_name.'%']];
        $allArr = BookPublish::where($whereArr)->paginate(10);

        foreach($allArr as $key => $val) {

            $countManga = BookPublish::withCount('manga','mangaDetail')
            ->where([['id','=',$val->id]])->first();

            $allArr[$key]->mangaSeriesCount = $countManga->manga_count;
            $allArr[$key]->mangaBooksCount = $countManga->manga_detail_count;


            $whereArr = [['publishID','=',$val->id]];

            $allArr[$key]->novelSeriesCount =  Novel::where($whereArr)->count();
            $allArr[$key]->novelBooksCount = $novelModel->countNovelDetail($whereArr);

            $allArr[$key]->ceCount = ComicEssay::where($whereArr)->count();

        }

        $this->data = null;
        $this->data['allArr'] = $allArr;
        $this->data['search_name'] = $search_name;

        return view('bookpublish.index',$this->data);
    }

    public function view($id) {

        $publishInfo = BookPublish::find($id);

        $this->data = null;
        $this->data['name'] = $publishInfo->name;
        $this->data['mangaList'] = Manga::with('oriPublish')->where('publishID',$id)->get();
        $this->data['novelList'] = Novel::where('publishID',$id)->get();
        $this->data['ceList'] = ComicEssay::where('publishID',$id)->get();
        

        return view('bookpublish.view',$this->data);
    }
    
    public function edit(Request $request,$id) {
        $params = $request->all();
        //Session::forget('flash_message');

        //echo '<pre>';print_r($params);echo '</pre>';
        //Book::find($id)->update([ 'author_id' => 10]);

        $modelOjb = BookPublish::find($id);

        if(isset($params['mode'])) {
            if($params['mode'] == 'update') {

                $validatedData = $request->validate([
                    'name' => 'required',
                ], $this->errorMessages() );

                $modelOjb->name = $request->name;
                $modelOjb->save();

                Session::flash('flash_message', 'updated');
            }
        } 

        $this->data = null;
        $this->data['arrInfo'] = $modelOjb;
        return view('bookpublish.edit',$this->data);
    }
    
    public function insert(Request $request) {

        $params = $request->all();

        $modelObj = new BookPublish;

        if(isset($params['mode'])) {
            if($params['mode'] == 'insert') { 

                $validatedData = $request->validate([
                    'name' => 'required',
                ], $this->errorMessages() );

                $modelObj->name = $request->name;
                $modelObj->save();
                Session::flash('flash_message', 'inserted');
                return redirect()->route('publish_edit', $modelObj->id);
            }
        }

        return view('bookpublish.insert');
    }

    public function errorMessages()
    {
        return [
            'name.required' => 'Publish name is required',
        ];
    }    

}
