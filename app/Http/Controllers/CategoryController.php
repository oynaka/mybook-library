<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Config;
use Session;
use View;
use Route;
use App\Category;
use App\BookCategory;

class CategoryController extends AuthenController {
    
    private $data = array();

    public function __construct(Request $request) {
        parent::__construct();
    }
    
    public function init(Request $request)
    {
        $params = $request->all();

        $search_name = '';
        if(isset($params['search_name'])) {
            $search_name = $params['search_name'];
        }

        //echo '<pre>';print_r($params);echo '</pre>';
        //echo '<pre>';print_r(session()->all());echo '</pre>';

        $whereArr = null;
        $whereArr = [['catName', 'like', '%'.$search_name.'%']];
        $category = Category::where($whereArr)->paginate(10);
        //echo '<pre>';print_r($category);echo '</pre>';

        $this->data = null;
        $this->data['category_list'] = $category;
        $this->data['search_name'] = $search_name;

        return view('category.index',$this->data);
    }

    public function edit(Request $request,$cat_id) {
        $categoryInfo = Category::find($cat_id);
        $params = $request->all();

        //get all manga in this category
        $mangaList = BookCategory::with('manga')
                                   ->where('catID',$cat_id)
                                   ->get();

        if(isset($params['mode'])) {
            if($params['mode'] == 'update') {

                $validatedData = $request->validate([
                    'catName' => 'required',
                ], $this->errorMessages() );

                $categoryInfo->catName = $request->catName;
                $categoryInfo->catRemark = $request->catRemark;
                $categoryInfo->save();

                Session::flash('flash_message', 'updated');
            }
        } else {
            if(Session::get('flash_message') == 'updated') {
                Session::forget('flash_message');
            }
        }
        
        $this->data = null;
        $this->data['categoryInfo'] = $categoryInfo;
        $this->data['mangaList'] = $mangaList;
        
        return view('category.edit',$this->data);
    }

    public function errorMessages()
    {
        return [
            'catName.required' => 'Category name is required',
        ];
    }        

    public function insert(Request $request) {

        $params = $request->all();
        //echo '<pre>';print_r($params);echo '</pre>';

        $categoryModel = new Category;

        if(isset($params['mode'])) {
            if($params['mode'] == 'insert') { 

                $validatedData = $request->validate([
                    'catName' => 'required',
                ], $this->errorMessages() );

                $categoryModel->catName = $request->catName;
                $categoryModel->catRemark = $request->catRemark;
                $categoryModel->save();
                Session::flash('flash_message', 'inserted');
                return redirect()->route('category_edit', $categoryModel->catID);
            }
        }

        return view('category.insert');
    }

}
