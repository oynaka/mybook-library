<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Collection;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Config;
use Session;
use View;
use Route;
use App\ComicEssay;
use App\ComicEssayDetail;
use App\Author;
use App\AuthorWorkLnk;
use App\BookLocation;
use App\BookCategory;
use App\Translator;
use App\BookPublish;
use App\OriginalPublish;
use App\Category;

class ComicEssayController extends AuthenController {
    
    private $data = array();
    private $book_type = 3;

    public function __construct(Request $request) {
        parent::__construct();
        
    }
    
    public function init(Request $request)
    {
        
        $ceModel = new ComicEssay();
        $params = $request->all();

        $search_name = '';
        if(isset($params['search_name'])) {
            $search_name = $params['search_name'];
        }

        $author_id = '';
        if(isset($params['author_id'])) {
            $author_id = $params['author_id'];
        }

        $ce_arr = $ceModel->queryAllComicEssay($search_name,$author_id);

        //create manualy pagination
        $page = 1;
        if(isset($params['page'])) {
            $page = $params['page'];
        } 
        $limit = 10;

        $paginator = new LengthAwarePaginator($ce_arr->forPage($page, $limit), $ce_arr->count(), $limit, $page );
        $paginator->withPath('comicessay');

        $itemPerPage = $paginator->items();
        //$dataArray = $paginator->appends('filter', 'aabb');

        foreach($itemPerPage as $objItem) {
            $objItem->latest_vol = ComicEssayDetail::where('ceID',$objItem->ceID)->max('vol');

            $objItem->author = AuthorWorkLnk::with('authors')
                                    ->where('type',$this->book_type)
                                    ->where('role_id',1)
                                    ->where('book_id',$objItem->ceID)
                                    ->get();
        }

        /*
        //echo '<pre>';print_r($params);echo '</pre>';
        //echo '<pre>';print_r(session()->all());echo '</pre>';
        */

        $this->data = null;
        $this->data['item_arr'] = $itemPerPage;
        $this->data['paginator'] = $paginator;
        $this->data['all_author'] = Author::select('id','name')->orderBy('name')->get();
        $this->data['search_name'] = $search_name;
        $this->data['author_id'] = $author_id;

        return view('comicessay.index',$this->data);
    }

    public function view(Request $request,$ceID) {

        $params = $request->all();
        //echo '<pre>';print_r($params);echo '</pre>';exit();
        
        if(isset($params['mode'])) {
            //echo '<pre>';print_r($params);echo '</pre>';exit();
            switch($params['mode']) {
                case 'edit'       : $this->save_ce($ceID,$params);
                                      break;
                case 'newVol'       : $this->insert_new_vol($ceID,$params);
                                      break;
                case 'allLoc'       : $this->update_all_location($ceID,$params);
                                      break;
                case 'editDetail'   : $this->edit_ce_detail($ceID,$params);
                                      break;
            }
            
        } else {
            Session::forget('flash_message');
        }

        $bookInfo = ComicEssay::find($ceID);

        $authors = AuthorWorkLnk::with('authors')
                                ->where('type',$this->book_type)
                                ->where('role_id',1)
                                ->where('book_id',$ceID)
                                ->get();

        $bookDetail = ComicEssayDetail::where('ceID',$ceID)
                        ->orderBy('vol','asc')
                        ->get();

        $latestVol = $bookDetail->last()->vol;
        $latestPrice = $bookDetail->last()->price;

        //echo '<pre>';print_r(Category::all());echo '</pre>';
        $this->data = null;
        $this->data['bookInfo'] = $bookInfo;
        $this->data['bookDetail'] = $bookDetail;
        $this->data['authors'] = $authors;
        $this->data['publish'] = BookPublish::find($bookInfo->publishID);
        $this->data['allPublish'] = BookPublish::all();
        $this->data['allStatus'] = ComicEssay::$bookStatus;
        $this->data['status'] = ComicEssay::getStatus($bookInfo->status);
        $this->data['nextVol'] = intval($latestVol)+1;
        $this->data['latestPrice'] = $latestPrice;
        $this->data['currentDate'] = date('Y-m-d');
        $this->data['allLocation'] = BookLocation::all();
        $this->data['allTranslator'] = Translator::orderBy('translator_name','asc')->get();
        
        return view('comicessay.view',$this->data);
    }

    public function save_ce($ceID,$data) {

        //echo '<pre>';print_r($data);echo '</pre>';exit();

        //save ce table
        $obj = ComicEssay::find($ceID);
        $obj->ceName = $data['ceName'];
        $obj->ceNameEng = $data['ceNameEng'];
        $obj->native_name = $data['ceNativeName'];
        $obj->publishID = $data['publishID'];
        $obj->status = $data['status'];
        $obj->save();

        //save authorWorkLnk table
        $authorLnkOjb = new AuthorWorkLnk;
        $authorLnkOjb->updateBookAuthor($ceID,$data['author_id'],$this->book_type,1);

        Session::flash('flash_message', 'updated');
    }

    private function insert_new_vol($ceID,$data) {
        $obj = new ComicEssayDetail;
        $obj->ceID = $ceID;
        $obj->vol = $data['vol'];
        $obj->quan = 1;
        $obj->price = $data['price'];
        $obj->date_buy = $data['date_buy'];
        $obj->save();
        Session::flash('flash_message', 'new_vol_inserted');
    }

    private function update_all_location($ceID,$data) {
        $result = ComicEssayDetail::where('ceID',$ceID)
                            ->update(['location' => $data['all_location']]);
        Session::flash('flash_message', 'all_loc_updated');
    }

    public function edit_ce_detail($ceID,$data) {

       // echo '<pre>';print_r($data);echo '</pre>';exit();

        $obj = ComicEssayDetail::find($data['ceDetailID']);
        $obj->location = $data['detail_location'];
        $obj->date_buy = $data['detail_date_buy'];
        $obj->transID = $data['detail_translator'];

        if(isset($data['detail_read_flg'])) {
            $obj->readFlg = $data['detail_read_flg'];
        } else {
            $obj->readFlg = 0;
        }

        $obj->save();

        Session::flash('flash_message', 'detail_updated');
    }

    public function insert(Request $request) {

        if(isset($request)) {
            if($request->mode == 'insert') { 

                $validatedData = $request->validate([
                    'ceName' => 'required',
                ], $this->errorMessages() );

                $params = $request->all();
                $obj = new ComicEssay;
                $ceID = $obj->insertCE($params);
                return redirect()->route('comicessay_view', $ceID);
            }
        }

        //echo '<pre>';print_r($params);echo '</pre>';

        $this->data = null;
        $this->data['currentDate'] = date('Y-m-d');
        $this->data['allPublish'] = BookPublish::all();

        return view('comicessay.insert',$this->data);
    }
    
    public function errorMessages()
    {
        return [
            'ceName.required' => 'Comic Essay name is required'
        ];
    }    


}
