<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Config;
use Session;
use View;
use Route;
use App\Manga;
use App\MangaDetail;
use App\Novel;
use App\NovelDetail;
use App\ComicEssay;

class DashboardController extends AuthenController {
    
    private $data = array();

    public function __construct(Request $request) {
        parent::__construct();
    }

    
    public function init()
    {
        //echo '<pre>';print_r(session()->all());echo '</pre>';
        $all_manga = $this->getAllManga();
        $all_novel = $this->getAllNovel();
        $all_ce = $this->getAllCe();

        $this->data = null;
        $this->data['unread_book'] = $this->getUnreadBooks();
        $this->data['new_book'] = $this->getNewBooks();
        $this->data['all_manga'] = $all_manga;
        $this->data['all_novel'] = $all_novel;
        $this->data['all_ce'] = $all_ce;

        return view('dashboard',$this->data);
    }

    private function getAllManga() {
        $obj = new Manga;
        $count = 0;

        $whereArr = [['mangaStatus','!=','S']];
        $count = $obj->countMangaDetail($whereArr);

        return $count;
    }

    private function getAllNovel() {
        $obj = new Novel;
        $count = 0;

        $whereArr = [['novelStatus','!=','S']];
        $count = $obj->countNovelDetail($whereArr);

        return $count;
    }    

    private function getAllCe() {
        $obj = new ComicEssay;
        $count = 0;

        $whereArr = [['status','!=','S']];
        $count = $obj->countCEDetail($whereArr);

        return $count;
    }       
    
    public function getUnreadBooks() {
        $mangaModel = new Manga;
        $novelModel = new Novel;

        $whereArr = [['readFlg',0]];
        $count = 0;
        $count += $mangaModel->countMangaDetail($whereArr);
        $count += $novelModel->countNovelDetail($whereArr);

        return $count;
    }

    public function getNewBooks() {

        $cManga = MangaDetail::whereRaw('DATEDIFF(mangaDateBuy,(select max(mangaDateBuy) from mangadetail)) between -90 and 0')
                            ->count();

        $cNovel = NovelDetail::whereRaw('DATEDIFF(novelDateBuy,(select max(novelDateBuy) from noveldetail)) between -90 and 0')
                            ->count();

        $count = $cManga+$cNovel;

        return $count;
    }


}
