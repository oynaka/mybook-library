<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Session;
use View;
use Route;

class HomeController extends AuthenController {
    
    private $data = array();

    public function __construct(Request $request) {
        parent::__construct();
    }

    public function home() {
        return view('home', $this->data);
    }

}
