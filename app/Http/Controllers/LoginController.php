<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Session;
use Redirect;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\AdminUsers;

class LoginController extends Controller {

    private $data = array();

    public function init(Request $request) {
        $params = $request->all();

        if (isset($params) && isset($params['login_id']) && isset($params['password'])) {
            
            $adminModel = new AdminUsers;

            $user_data = $adminModel->getDataByLoginID($params['login_id']);

            if (count($user_data) > 0) {
                if( md5($params['password']) == $user_data[0]->password ) {

                    Session::put('user_id', $user_data[0]->user_id);

                    return Redirect::route('dashboard');
                } else {
                    Session::flash('flash_message', 'login_error');
                }
            } else {
                Session::flash('flash_message', 'login_error');
            }

            $this->data['arrForm'] = $params;
           
        }

        return view('login', $this->data);
    }

}
