<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Session;
use Redirect;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class LogoutController extends Controller
{

    public function init(Request $request)
    {
    Session::forget('user_id');

    return Redirect::route('login');
    }
}
