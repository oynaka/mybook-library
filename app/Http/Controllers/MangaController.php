<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Pagination\LengthAwarePaginator;
//use Illuminate\Support\Collection;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Config;
use Session;
use View;
use Route;
use App\Manga;
use App\MangaDetail;
use App\Author;
use App\AuthorWorkLnk;
use App\BookLocation;
use App\BookCategory;
use App\Translator;
use App\BookPublish;
use App\OriginalPublish;
use App\Category;

class MangaController extends AuthenController {
    
    private $data = array();
    private $book_type = 1;

    public function __construct(Request $request) {
        parent::__construct();
        
    }
    
    public function init(Request $request)
    {
        
        $mangaModel = new Manga();
        $params = $request->all();

        $search_name = '';
        if(isset($params['search_name'])) {
            $search_name = $params['search_name'];
        }

        $author_id = '';
        if(isset($params['author_id'])) {
            $author_id = $params['author_id'];
        }

        $manga_arr = $mangaModel->queryAllManga($search_name,$author_id);

        //create manualy pagination
        $page = 1;
        if(isset($params['page'])) {
            $page = $params['page'];
        } 
        $limit = 10;

        $paginator = new LengthAwarePaginator($manga_arr->forPage($page, $limit), $manga_arr->count(), $limit, $page );
        $paginator->withPath('manga');

        $itemPerPage = $paginator->items();
        //$dataArray = $paginator->appends('filter', 'aabb');

        foreach($itemPerPage as $objItem) {
            $objItem->latest_vol = MangaDetail::where('mangaID',$objItem->mangaID)
                                                ->max('mangaVol');

            $objItem->author = AuthorWorkLnk::with('authors')
                                        ->where('type',$this->book_type)
                                        ->where('book_id',$objItem->mangaID)
                                        ->get();
        }

        /*
        //echo '<pre>';print_r($params);echo '</pre>';
        //echo '<pre>';print_r(session()->all());echo '</pre>';

        $whereArr = null;
        $whereArr = [['mangaName', 'like', '%'.$search_name.'%']];
        $manga_arr = Manga::where($whereArr)->paginate(10);
        
        */
        
        //echo '<pre>';print_r();echo '</pre>';
        //echo 'first item = '.$paginator->firstitem();

        $this->data = null;
        $this->data['manga_arr'] = $itemPerPage;
        $this->data['paginator'] = $paginator;
        $this->data['all_author'] = Author::select('id','name')->orderBy('name')->get();
        $this->data['search_name'] = $search_name;
        $this->data['author_id'] = $author_id;

        return view('manga.index',$this->data);
    }

    public function view(Request $request,$mangaID) {

        $params = $request->all();
        //echo '<pre>';print_r($params);echo '</pre>';
        
        if(isset($params['mode'])) {
            //echo '<pre>';print_r($params);echo '</pre>';exit();
            switch($params['mode']) {
                case 'edit'       : $this->saveManga($mangaID,$params);
                                      break;
                case 'newVol'       : $this->insertNewVol($mangaID,$params);
                                      break;
                case 'allLoc'       : $this->updateAllLocation($mangaID,$params);
                                      break;
                case 'editDetail'   : $this->editMangaDetail($mangaID,$params);
                                      break;
            }
            
        } else {
            Session::forget('flash_message');
        }

        $mangaInfo = Manga::find($mangaID);

        $authorArr = AuthorWorkLnk::with('authors')
                                ->where('type',$this->book_type)
                                ->where('book_id',$mangaID)
                                ->get();

        $authors = array();
        $artists = array();
        $chardesigns = array();
        foreach($authorArr as $item) {
            if($item->role_id == 1) {
                $authors[] = $item;
            } else if ($item->role_id == 2) {
                $artists[] = $item;
            } else {
                $chardesigns[] = $item;
            }
        }

        //echo '<pre>';print_r($artists);echo '</pre>';
        //echo '<pre>';print_r($temp->toArray());echo '</pre>';

        $mangaDetail = MangaDetail::with('location','translator')
                        ->where('mangaID',$mangaID)
                        ->orderBy('mangaVol','asc')
                        ->get();

        $latestVol = $mangaDetail->last()->mangaVol;
        $latestPrice = $mangaDetail->last()->mangaPrice;

        //echo '<pre>';print_r(Category::all());echo '</pre>';
        $this->data = null;
        $this->data['mangaInfo'] = $mangaInfo;
        $this->data['mangaDetail'] = $mangaDetail;
        $this->data['authorArr'] = $authorArr;
        $this->data['authors'] = $authors;
        $this->data['artists'] = $artists;
        $this->data['chardesigns'] = $chardesigns;
        $this->data['publish'] = BookPublish::find($mangaInfo->publishID);
        $this->data['allPublish'] = BookPublish::all();
        $this->data['allOriPublish'] = OriginalPublish::orderBy('name','asc')->get();
        $this->data['oriPublish'] = OriginalPublish::find($mangaInfo->oriPublishID);
        $this->data['allStatus'] = Manga::$mangaStatus;
        $this->data['status'] = Manga::getMangaStatus($mangaInfo->mangaStatus);
        $this->data['allCategory'] = Category::all();
        $this->data['bookCategory'] = $mangaInfo->category;
        $this->data['nextVol'] = intval($latestVol)+1;
        $this->data['latestPrice'] = $latestPrice;
        $this->data['currentDate'] = date('Y-m-d');
        $this->data['allLocation'] = BookLocation::all();
        $this->data['allTranslator'] = Translator::orderBy('translator_name','asc')->get();
        
        return view('manga.view',$this->data);
    }

    public function saveManga($mangaID,$data) {

        //echo '<pre>';print_r($data);echo '</pre>';exit();
        
        //save manga table
        $manga = Manga::find($mangaID);
        $manga->mangaName = $data['mangaName'];
        $manga->mangaNameEng = $data['mangaNameEng'];
        $manga->mangaNativeName = $data['mangaNativeName'];
        $manga->publishID = $data['publishID'];
        $manga->oriPublishID = $data['oriPublishID'];
        $manga->mangaStatus = $data['mangaStatus'];
        $manga->externalWebID = $data['externalWebID'];
        $manga->remark = $data['remark'];
        $manga->save();

        //save authorWorkLnk table
        $authorLnkOjb = new AuthorWorkLnk;

        $author_id_box = array_unique($data['author_id_box']);
        foreach($author_id_box as $author) {
            $authorArr[] = [ 'type' => $this->book_type , 
                             'role_id' => 1 , 
                             'book_id' => $mangaID,
                             'author_id' => $author
                           ];
        }

        $artist_id_box = array_unique($data['artist_id_box']);
        foreach($artist_id_box as $author) {
            $authorArr[] = [ 'type' => $this->book_type , 
                             'role_id' => 2 , 
                             'book_id' => $mangaID,
                             'author_id' => $author
                           ];
        }

        if(isset($data['char_id_box'])) {
            $char_id_box = array_unique($data['char_id_box']);
            foreach($char_id_box as $author) {
                $authorArr[] = [ 'type' => $this->book_type , 
                                 'role_id' => 3 , 
                                 'book_id' => $mangaID,
                                 'author_id' => $author
                               ];
            }
        }
        
        $authorLnkOjb->updateMangaAuthor($mangaID,$authorArr);

        //book category
        $deletedRows = BookCategory::where('bookID', $mangaID)
                                    ->where('bookTypeID',1)
                                    ->delete();

        BookCategory::resetAutoIncrement();

        if(isset($data['manga_category'])) {
            if(count($data['manga_category']) > 0) {
                foreach($data['manga_category'] as $catID) {
                    $obj = new BookCategory;
                    $obj->bookID = $mangaID;
                    $obj->catID = $catID;
                    $obj->bookTypeID = 1;
                    $obj->save();
                }
            }
        }

        Session::flash('flash_message', 'updated');

    }

    private function insertNewVol($mangaID,$data) {
        $obj = new MangaDetail;
        $obj->mangaID = $mangaID;
        $obj->mangaVol = $data['vol'];
        $obj->mangaQuan = 1;
        $obj->mangaPrice = $data['price'];
        $obj->mangaDateBuy = $data['date_buy'];
        $obj->save();
        Session::flash('flash_message', 'new_vol_inserted');
    }

    private function updateAllLocation($mangaID,$data) {
        $result = MangaDetail::where('mangaID',$mangaID)
                            ->update(['mangaLocation' => $data['all_location']]);
        Session::flash('flash_message', 'all_loc_updated');
    }

    public function editMangaDetail($mangaID,$data) {
        $mangaDetail = MangaDetail::find($data['mangaDetailID']);
        $mangaDetail->mangaLocation = $data['detail_location'];
        $mangaDetail->mangaDateBuy = $data['detail_date_buy'];
        $mangaDetail->transID = $data['detail_translator'];
        $mangaDetail->mangaPhoto =  $data['detail_photo'];

        if(isset($data['detail_read_flg'])) {
            $mangaDetail->readFlg = $data['detail_read_flg'];
        } else {
            $mangaDetail->readFlg = 0;
        }

        $mangaDetail->save();

        Session::flash('flash_message', 'detail_updated');
    }

    public function insert(Request $request) {

        if(isset($request)) {
            if($request->mode == 'insert') { 

                $validatedData = $request->validate([
                    'mangaName' => 'required',
                    'mangaNameEng' => 'required',
                ], $this->errorMessages() );

                $params = $request->all();
                $obj = new Manga;
                $mangaID = $obj->insertManga($params);
                return redirect()->route('manga_view', $mangaID);
            }
        }

        //echo '<pre>';print_r($params);echo '</pre>';

        $this->data = null;
        $this->data['currentDate'] = date('Y-m-d');
        $this->data['allPublish'] = BookPublish::all();
        $this->data['allOriPublish'] = OriginalPublish::orderBy('name','asc')->get();

        return view('manga.insert',$this->data);
    }

    public function errorMessages()
    {
        return [
            'mangaName.required' => 'Manga name is required',
            'mangaNameEng.required' => 'Manga name Eng is required',
        ];
    }    



    /*
สมมติเพนมี
$shops = DB::table('shops')
           ->leftJoin('users', 'shops.account_id', '=', 'users.id')
           ->leftJoin('shop_types', 'shops.shop_type', '=', 'shop_types.id')
           ->leftJoin('shop_categories', 'shops.shop_category', '=', 'shop_categories.id')
           ->leftJoin('floor_zone_mappings', 'shops.zone_mapping_id', '=', 'floor_zone_mappings.id')
           ->leftJoin('floors', 'floor_zone_mappings.floor_id', '=', 'floors.id')
           ->leftJoin('zones', 'floor_zone_mappings.zone_id', '=', 'zones.id')
           ->select('shops.*',
               'users.id as account_id',
               'users.name as account_name',
               'shop_types.name as type_name',
               'shop_categories.name as category_name',
               'zones.name as zone_name',
               'floors.name as floor_name'
           )
           ->get();

$limit = intval(Input::get('limit', 10));
$page = intval(Input::get('page', 1));
$paginator = new LengthAwarePaginator($shops->forPage($page, $limit), $shops->count(), $limit, $page, []);
$dataArray = $paginator->items();
เป็นการสร้าง pagination แบบ custom เอง
โดยใช้  use Illuminate\Pagination\LengthAwarePaginator;
ตอนที่จะ return ไปให้ front ก็ใช้แบบนี้ก็ได้
[
               'rows' => $dataArray,
               'pagination' => [
                   'total_items' => $paginator->total(),
                   'current_page_items' => count($dataArray),
                   'items_per_page' => $paginator->perPage(),
                   'current_page' => $paginator->currentPage(),
                   'total_pages' => $paginator->lastPage(),
               ]
ได้ผลเหมือนกันกับ ::paginate(10)

    */



}
