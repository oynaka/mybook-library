<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Config;
use Session;
use View;
use Route;
use App\Manga;
use App\Novel;
use App\MangaDetail;
use App\NovelDetail;

class NewBookController extends AuthenController {
    
    private $data = array();

    public function __construct(Request $request) {
        parent::__construct();
        
    }
    
    public function init(Request $request)
    {

        $mangaModel = new Manga();
        $novelModel =  new Novel();

        //$params = $request->all();
        //echo '<pre>';print_r($params);echo '</pre>';
        //echo '<pre>';print_r(session()->all());echo '</pre>';

        $mangaList = $mangaModel->getNewManga();
        $novelList = $novelModel->getNewNovel();

        //echo '<pre>';print_r($mangaList);echo '</pre>';

        $this->data = null;
        $this->data['mangaList'] = $mangaList;
        $this->data['novelList'] = $novelList;
        $this->data['totalManga'] = count($mangaList);
        $this->data['totalNovel'] = count($novelList);;

        return view('newbook.index',$this->data);
    }

    public function showRawData() {
        $mangaModel = new Manga();
        $novelModel =  new Novel();

        $mangaList = $mangaModel->getNewRawManga();
        $novelList = $novelModel->getNewNovel();

        $mangaArr = null;
        foreach($mangaList as $manga) {
            $mangaArr[$manga->mangaDateBuy][] = $manga;
        }

        $novelArr = null;
        foreach($novelList as $novel) {
            $novelArr[$novel->novelDateBuy][] = $novel;
        }

        $this->data = null;
        $this->data['mangaList'] = $mangaArr;
        $this->data['novelList'] = $novelArr;
        $this->data['totalManga'] = count($mangaList);
        $this->data['totalNovel'] = count($novelList);;

        return view('newbook.raw',$this->data);
    }

}
