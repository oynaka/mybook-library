<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Pagination\LengthAwarePaginator;
//use Illuminate\Support\Collection;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Config;
use Session;
use View;
use Route;
use App\Novel;
use App\NovelDetail;
use App\Author;
use App\AuthorWorkLnk;
use App\BookLocation;
use App\BookCategory;
use App\Translator;
use App\BookPublish;
use App\OriginalPublish;
use App\Category;

class NovelController extends AuthenController {
    
    private $data = array();
    private $book_type = 2;

    public function __construct(Request $request) {
        parent::__construct();
        
    }
    
    public function init(Request $request)
    {
        
        $novelModel = new Novel();
        $params = $request->all();

        $search_name = '';
        if(isset($params['search_name'])) {
            $search_name = $params['search_name'];
        }

        $author_id = '';
        if(isset($params['author_id'])) {
            $author_id = $params['author_id'];
        }        

        $novel_arr = $novelModel->queryAllNovel($search_name,$author_id);

        //create manualy pagination
        $page = 1;
        if(isset($params['page'])) {
            $page = $params['page'];
        } 
        $limit = 10;

        $paginator = new LengthAwarePaginator($novel_arr->forPage($page, $limit), $novel_arr->count(), $limit, $page );
        $paginator->withPath('novel');

        $itemPerPage = $paginator->items();
        //$dataArray = $paginator->appends('filter', 'aabb');

        foreach($itemPerPage as $objItem) {
            $objItem->latest_vol = NovelDetail::where('novelID',$objItem->novelID)
                                                ->max('novelVol');

            $objItem->author = AuthorWorkLnk::with('authors')
                                        ->where('type',$this->book_type)
                                        ->where('book_id',$objItem->novelID)
                                        ->where('role_id',1)
                                        ->get();
        }

        /*
        //echo '<pre>';print_r($params);echo '</pre>';
        //echo '<pre>';print_r(session()->all());echo '</pre>';
        //echo '<pre>';print_r($allBookLocation);echo '</pre>';
        */

        $this->data = null;
        $this->data['novel_arr'] = $itemPerPage;
        $this->data['paginator'] = $paginator;
        $this->data['all_author'] = Author::select('id','name')->orderBy('name')->get();
        $this->data['search_name'] = $search_name;
        $this->data['author_id'] = $author_id;

        return view('novel.index',$this->data);
    }

    public function view(Request $request,$novelID) {

        $params = $request->all();
        //echo '<pre>';print_r($params);echo '</pre>';exit();
        
        if(isset($params['mode'])) {
            //echo '<pre>';print_r($params);echo '</pre>';exit();
            switch($params['mode']) {
                case 'edit'       : $this->saveNovel($novelID,$params);
                                      break;
                case 'newVol'       : $this->insertNewVol($novelID,$params);
                                      break;
                case 'allLoc'       : $this->updateAllLocation($novelID,$params);
                                      break;
                case 'editDetail'   : $this->editNovelDetail($novelID,$params);
                                      break;
            }
            
        } else {
            Session::forget('flash_message');
        }

        $novelInfo = Novel::find($novelID);

        $authors = AuthorWorkLnk::with('authors')
                                ->where('type',$this->book_type)
                                ->where('role_id',1)
                                ->where('book_id',$novelID)
                                ->get();

        $novelDetail = NovelDetail::where('novelID',$novelID)
                        ->orderBy('novelVol','asc')
                        ->get();

        $latestVol = $novelDetail->last()->novelVol;
        $latestPrice = $novelDetail->last()->novelPrice;


        //echo '<pre>';print_r(Category::all());echo '</pre>';
        $this->data = null;
        $this->data['novelInfo'] = $novelInfo;
        $this->data['novelDetail'] = $novelDetail;
        $this->data['authors'] = $authors;
        $this->data['publish'] = BookPublish::find($novelInfo->publishID);
        $this->data['allPublish'] = BookPublish::all();
        $this->data['allStatus'] = Novel::$novelStatus;
        $this->data['status'] = Novel::getNovelStatus($novelInfo->novelStatus);
        $this->data['nextVol'] = intval($latestVol)+1;
        $this->data['latestPrice'] = $latestPrice;
        $this->data['currentDate'] = date('Y-m-d');
        $this->data['allLocation'] = BookLocation::all();
        $this->data['allTranslator'] = Translator::orderBy('translator_name','asc')->get();
        
        return view('novel.view',$this->data);
    }

    public function saveNovel($novelID,$data) {

        //echo '<pre>';print_r($data);echo '</pre>';exit();
        
        //save novel table
        $novel = Novel::find($novelID);
        $novel->novelName = $data['novelName'];
        $novel->novelNameEng = $data['novelNameEng'];
        $novel->novelNativeName = $data['novelNativeName'];
        $novel->publishID = $data['publishID'];
        $novel->novelStatus = $data['novelStatus'];
        $novel->save();

        //save authorWorkLnk table
        $authorLnkOjb = new AuthorWorkLnk;
        $authorLnkOjb->updateNovelAuthor($novelID,$data['author_id']);

        Session::flash('flash_message', 'updated');
    }

    private function insertNewVol($novelID,$data) {
        $obj = new NovelDetail;
        $obj->novelID = $novelID;
        $obj->novelVol = $data['vol'];
        $obj->novelQuan = 1;
        $obj->novelPrice = $data['price'];
        $obj->novelDateBuy = $data['date_buy'];
        $obj->save();
        Session::flash('flash_message', 'new_vol_inserted');
    }

    private function updateAllLocation($novelID,$data) {
        $result = NovelDetail::where('novelID',$novelID)
                            ->update(['novelLocation' => $data['all_location']]);
        Session::flash('flash_message', 'all_loc_updated');
    }

    public function editNovelDetail($novelID,$data) {
        $novelDetail = NovelDetail::find($data['novelDetailID']);
        $novelDetail->novelLocation = $data['detail_location'];
        $novelDetail->novelDateBuy = $data['detail_date_buy'];
        $novelDetail->transID = $data['detail_translator'];
        $novelDetail->novelPhoto = $data['detail_photo'];

        if(isset($data['detail_read_flg'])) {
            $novelDetail->readFlg = $data['detail_read_flg'];
        } else {
            $novelDetail->readFlg = 0;
        }

        $novelDetail->save();

        Session::flash('flash_message', 'detail_updated');
    }

    public function insert(Request $request) {

        if(isset($request)) {
            if($request->mode == 'insert') { 

                $validatedData = $request->validate([
                    'novelName' => 'required',
                ], $this->errorMessages() );

                $params = $request->all();
                $obj = new Novel;
                $novelID = $obj->insertNovel($params);
                return redirect()->route('novel_view', $novelID);
            }
        }

        //echo '<pre>';print_r($params);echo '</pre>';

        $this->data = null;
        $this->data['currentDate'] = date('Y-m-d');
        $this->data['allPublish'] = BookPublish::all();

        return view('novel.insert',$this->data);
    }
    
    public function errorMessages()
    {
        return [
            'novelName.required' => 'Novel name is required'
        ];
    }    


}
