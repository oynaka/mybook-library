<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Config;
use Session;
use View;
use Route;
use App\Translator;
use App\Manga;
use App\MangaDetail;
use App\NovelDetail;
use App\ComicEssayDetail;

class TranslatorController extends AuthenController {
    
    private $data = array();

    public function __construct(Request $request) {
        parent::__construct();
    }
    
    public function init(Request $request)
    {
        $params = $request->all();

        $search_name = '';
        if(isset($params['search_name'])) {
            $search_name = $params['search_name'];
        }

        //echo '<pre>';print_r($params);echo '</pre>';
        //echo '<pre>';print_r(session()->all());echo '</pre>';

        $whereArr = null;
        $whereArr = [['translator_name', 'like', '%'.$search_name.'%']];
        $translators = Translator::withCount('mangaDetail','novelDetail','ceDetail')
                                ->where($whereArr)
                                ->paginate(10);
        
        //echo '<pre>';print_r($translators);echo '</pre>';

        $this->data = null;
        $this->data['translators'] = $translators;
        $this->data['search_name'] = $search_name;

        return view('translator.index',$this->data);
    }

    public function edit(Request $request,$transID) {
        $translator = Translator::find($transID);
        $params = $request->all();

        //echo '<pre>';print_r($mangaList);echo '</pre>';

        $mangaList = MangaDetail::with('manga')
                        ->where('transID',$transID)
                        ->orderBy('mangaID','asc')
                        ->orderBy('mangaVol','asc')
                        ->get();

        $novelList = NovelDetail::with('novel')
                        ->where('transID',$transID)
                        ->orderBy('novelID','asc')
                        ->orderBy('novelVol','asc')
                        ->get();

        $ceList = ComicEssayDetail::with('ce')
                        ->where('transID',$transID)
                        ->orderBy('ceID','asc')
                        ->orderBy('Vol','asc')
                        ->get();

        if(isset($params['mode'])) {
            if($params['mode'] == 'update') {

                $validatedData = $request->validate([
                    'translator_name' => 'required',
                ], $this->errorMessages() );

                $translator->translator_name = $request->translator_name;
                $translator->remark = $request->remark;
                $translator->save();

                Session::flash('flash_message', 'updated');
            }
        } else {
            if(Session::get('flash_message') == 'updated') {
                Session::forget('flash_message');
            }
        }
        
        $this->data = null;
        $this->data['translator'] = $translator;
        $this->data['mangaList'] = $mangaList;
        $this->data['novelList'] = $novelList;
        $this->data['ceList'] = $ceList;
        
        return view('translator.edit',$this->data);
    }

    public function errorMessages()
    {
        return [
            'translator_name.required' => 'Translator name is required',
        ];
    }        

    public function insert(Request $request) {

        $params = $request->all();
        //echo '<pre>';print_r($params);echo '</pre>';

        $objModel = new Translator;

        if(isset($params['mode'])) {
            if($params['mode'] == 'insert') { 

                $validatedData = $request->validate([
                    'translator_name' => 'required',
                ], $this->errorMessages() );

                $objModel->translator_name = $request->translator_name;
                $objModel->remark = $request->remark;
                $objModel->save();
                Session::flash('flash_message', 'inserted');
                return redirect()->route('translator_edit', $objModel->transID);
            }
        }

        return view('translator.insert');
    }

}
