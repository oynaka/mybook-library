<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Config;
use Session;
use View;
use Route;
use App\MangaDetail;
use App\NovelDetail;

class UnreadController extends AuthenController {
    
    private $data = array();

    public function __construct(Request $request) {
        parent::__construct();
    }
    
    public function init(Request $request)
    {
        $params = $request->all();

        //echo '<pre>';print_r($params);echo '</pre>';
        //echo '<pre>';print_r(session()->all());echo '</pre>';

        $mangaList = MangaDetail::with('manga')
                        ->where('readFlg',0)
                        ->orderBy('mangaDateBuy','asc')
                        ->orderBy('mangaID','asc')
                        ->orderBy('mangaVol','asc')
                        ->get();

        $novelList = NovelDetail::with('novel')
                        ->where('readFlg',0)
                        ->orderBy('novelDateBuy','asc')
                        ->orderBy('novelID','asc')
                        ->orderBy('novelVol','asc')
                        ->get();

        //echo '<pre>';print_r($mangaList);echo '</pre>';exit();

        $this->data = null;
        $this->data['mangaList'] = $mangaList;
        $this->data['novelList'] = $novelList;

        return view('unread.index',$this->data);
    }

    public function edit(Request $request,$bookDetailID) {
        $params = $request->all();
        
        if($params['t'] == 1) {
            $modelObj = MangaDetail::find($bookDetailID);
            $modelObj->readFlg = 1;
            $modelObj->readFlgDate = date('Y-m-d');
            $modelObj->save();
        } else if ($params['t'] == 2) {
            $modelObj = NovelDetail::find($bookDetailID);
            $modelObj->readFlg = 1;
            $modelObj->readFlgDate = date('Y-m-d');
            $modelObj->save();
        }

        return redirect()->route('unread');
    }


}
