<?php
namespace App\Library;

class Queryhelper {
    
    public function bindParams($table, $params) {
        foreach ($params as $key => $value) {
            if ($value != '') {
                switch ($key) {
                    default:
                        $table->where($key, '=', $value);
                }
            }
        }

        return $table;
    }

}

?>