<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Library\Queryhelper;
use DB;

class Manga extends Model {

    protected $table = 'manga';
    protected $primaryKey = 'mangaID';
    const CREATED_AT = 'createdTime';
    const UPDATED_AT = 'updatedTime';

    static $mangaStatus = [ ['id' => 'C','name' => 'Complete'] ,
                            ['id' => 'O','name' => 'Ongoing'] ,
                            ['id' => 'H','name' => 'Hitius'] ,
                            ['id' => 'S','name' => 'Sold'] ];
    
    static function getMangaStatus($status) {
        $output = '';
        switch($status) {
            case 'C' : $output = 'Complete';break;
            case 'O' : $output = 'Ongoing';break;
            case 'H' : $output = 'Hitius';break;
            case 'S' : $output = 'Sold';break;
        }
        return $output;
    }

    public function insertManga($data) {

        $mangaID = DB::transaction(function() use ($data) {
            //echo '<pre>';print_r($data);echo '</pre>';
            
            $mangaID = DB::table($this->table)->insertGetId(
                            [
                              'mangaCode' => $data['mangaCode'],
                              'mangaName' => $data['mangaName'],
                              'mangaNameEng' => $data['mangaNameEng'],
                              'mangaNativeName' => $data['mangaNativeName'],
                              'publishID' => $data['publishID'],
                              'oriPublishID' => $data['oriPublishID'],
                              'mangaStatus' => $data['mangaStatus'],
                              'externalWebID' => $data['externalWebID'],
                              'createdTime' => now()
                            ]
            );
            
            //insert manga detail
            DB::table('mangadetail')->insert(
                [
                  'mangaID' => $mangaID,
                  'mangaVol' => $data['mangaVol'],
                  'mangaQuan' => 1,
                  'mangaPrice' => $data['mangaPrice'],
                  'mangaDateBuy' => $data['mangaDateBuy']
                ]
            );
            
            //insert author
            DB::table('authorworklnk')->insert(
                [
                  'book_id' => $mangaID,
                  'type' => 1,
                  'role_id' => 1,
                  'author_id' => $data['author_id']
                ]
            );

            //insert artist
            DB::table('authorworklnk')->insert(
                [
                  'book_id' => $mangaID,
                  'type' => 1,
                  'role_id' => 2,
                  'author_id' => $data['artist_id']
                ]
            );

            if(!empty($data['char_id'])) {
                //insert char design
                DB::table('authorworklnk')->insert(
                    [
                    'book_id' => $mangaID,
                    'type' => 1,
                    'role_id' => 3,
                    'author_id' => $data['char_id']
                    ]
                );
            }

            return $mangaID;
        });

        return $mangaID;
    }

    /*
    public function searchByParams($params = array()) {
        $select = DB::table($this->table)
            ->select($this->table . '.*');

        $queryHelper = new Queryhelper;

        $select = $queryHelper->bindParams($select, $params);

        return $select->get();
    }
    */

    public function queryAllManga($search_name=null,$author_id=null) {

        $search_author[] = ['authorworklnk.type','=',1];
        if(!empty($author_id)) {
            $search_author[] = ['authorworklnk.author_id','=',$author_id];
        }

        $manga = DB::table($this->table)
                 ->leftJoin('authorworklnk',$this->table.'.mangaID','authorworklnk.book_id')
                 ->whereRaw('(mangaName like "%'.$search_name.'%" or mangaNameEng like "%'.$search_name.'%")')
                 ->whereIn('role_id',[1, 2, 3])
                 ->where($search_author)
                 ->selectRaw('distinct '.$this->table.'.*')
                 ->get();

        return $manga;

    }

    public function getNewManga() {
        $select = DB::table($this->table)
                    ->leftJoin('mangadetail',$this->table.'.mangaID' , '=','mangadetail.mangaID')
                    ->selectRaw('*')
                    ->whereRaw('DATEDIFF(mangaDateBuy,(select max(mangaDateBuy) from mangadetail)) between -90 and 0')
                    ->orderBy('mangaDateBuy','desc')
                    ->orderBy($this->table.'.mangaID','asc')
                    ->orderBy('mangavol','asc');

        $data = $select->get();

        return $data;
    }

    public function getNewRawManga() {
        $select = DB::table($this->table)
                    ->leftJoin('mangadetail',$this->table.'.mangaID' , '=','mangadetail.mangaID')
                    ->selectRaw('*')
                    ->whereRaw('DATEDIFF(mangaDateBuy,(select max(mangaDateBuy) from mangadetail)) between -90 and 0')
                    ->whereRaw('(mangaLocation is null or mangaLocation != 9)')
                    ->orderBy('mangaDateBuy','desc')
                    ->orderBy($this->table.'.mangaID','asc')
                    ->orderBy('mangavol','asc');

        $data = $select->get();

        return $data;
    }

    public function mangaDetail() {
        return $this->hasMany('App\MangaDetail','mangaID');
    }

    public function countManga($whereArr=null) {
        $select = DB::table($this->table);

        if(!empty($whereArr)) {
            $select->where($whereArr);
        }

        $output = $select->count();

        return $output;
    }

    public function countMangaDetail($whereArr=null) {
        $select = DB::table($this->table)
                        ->leftJoin('mangadetail',$this->table.'.mangaID' , '=','mangadetail.mangaID');

        if(!empty($whereArr)) {
            $select->where($whereArr);
        }

        $output = $select->count();

        return $output;
    }

    public function getPublishMangaByYear($year) {

        $betweenArr = [$year."-01-01",$year."-12-31"];

        $select = DB::table($this->table)
                        ->leftJoin('mangadetail',$this->table.'.mangaID' , '=','mangadetail.mangaID')
                        ->selectRaw('publishID,count(*) as num')
                        ->whereBetween('mangaDatebuy',$betweenArr)
                        ->groupBy('publishID');

        $output = $select->get();

        return $output;
    }    

    public function category() {
        return $this->hasMany('App\BookCategory','bookID','mangaID')->where('book_categories.bookTypeID', '=', 1);
    }    

    public function publish() {
        return $this->belongsTo('App\BookPublish','publishID','id');
    }

    public function oriPublish() {
        return $this->belongsTo('App\BookOriPublish','oriPublishID','id');
    }
}
