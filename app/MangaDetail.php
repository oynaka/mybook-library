<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Library\Queryhelper;
use DB;

class MangaDetail extends Model {

    protected $table = 'mangadetail';
    protected $primaryKey = 'mangaDetailID';

    const CREATED_AT = 'createdTime';
    const UPDATED_AT = 'updatedTime';

    public function manga() {
        return $this->belongsTo('App\Manga','mangaID');
    }

    public function translator() {
        return $this->belongsTo('App\Translator','transID');
    }

    public function location() {
        return $this->belongsTo('App\BookLocation','mangaLocation');
    }
    
}
