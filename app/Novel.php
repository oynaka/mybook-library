<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Library\Queryhelper;
use DB;

class Novel extends Model {

    protected $table = 'novel';
    protected $primaryKey = 'novelID';
    const CREATED_AT = 'createdTime';
    const UPDATED_AT = 'updatedTime';

    static $novelStatus = [ ['id' => 'C','name' => 'Complete'] ,
                            ['id' => 'O','name' => 'Ongoing'] ,
                            ['id' => 'H','name' => 'Hitius'] ,
                            ['id' => 'S','name' => 'Sold'] ];
    
    static function getNovelStatus($status) {
        $output = '';
        switch($status) {
            case 'C' : $output = 'Complete';break;
            case 'O' : $output = 'Ongoing';break;
            case 'H' : $output = 'Hitius';break;
            case 'S' : $output = 'Sold';break;
        }
        return $output;
    }    

    public function queryAllNovel($search_name=null,$author_id=null) {

        $search_author[] = ['authorworklnk.type','=',2];
        if(!empty($author_id)) {
            $search_author[] = ['authorworklnk.author_id','=',$author_id];
        }

        $output = DB::table($this->table)
                 ->leftJoin('authorworklnk',$this->table.'.novelID','authorworklnk.book_id')
                 ->whereRaw('(novelName like "%'.$search_name.'%" or novelNameEng like "%'.$search_name.'%")')
                 ->whereIn('role_id',[1, 2, 3])
                 ->where($search_author)
                 ->selectRaw('distinct '.$this->table.'.*')
                 ->get();

        return $output;

    }    

    public function getNewNovel() {
        $select = DB::table($this->table)
                    ->leftJoin('noveldetail',$this->table.'.novelID' , '=','noveldetail.novelID')
                    ->selectRaw('*')
                    ->whereRaw('DATEDIFF(novelDateBuy,(select max(novelDateBuy) from noveldetail)) between -90 and 0')
                    ->orderBy('novelDateBuy','desc')
                    ->orderBy($this->table.'.novelID','asc')
                    ->orderBy('novelvol','asc');
                    
        $data = $select->get();

        return $data;
    }    

    public function insertNovel($data) {

        $novelID = DB::transaction(function() use ($data) {
            //echo '<pre>';print_r($data);echo '</pre>';
            
            $novelID = DB::table($this->table)->insertGetId(
                            [
                              'novelCode' => $data['novelCode'],
                              'novelName' => $data['novelName'],
                              'novelNameEng' => $data['novelNameEng'],
                              'novelNativeName' => $data['novelNativeName'],
                              'publishID' => $data['publishID'],
                              'novelStatus' => $data['novelStatus']
                            ]
            );
            
            //insert novel detail
            DB::table('noveldetail')->insert(
                [
                  'novelID' => $novelID,
                  'novelVol' => $data['novelVol'],
                  'novelQuan' => 1,
                  'novelPrice' => $data['novelPrice'],
                  'novelDateBuy' => $data['novelDateBuy']
                ]
            );
            
            //insert author
            DB::table('authorworklnk')->insert(
                [
                  'book_id' => $novelID,
                  'type' => 2,
                  'role_id' => 1,
                  'author_id' => $data['author_id']
                ]
            );

            return $novelID;
        });

        return $novelID;
    }  
    
    public function countNovelDetail($whereArr=null) {
        $select = DB::table($this->table)
                        ->leftJoin('noveldetail',$this->table.'.novelID' , '=','noveldetail.novelID');

        if(!empty($whereArr)) {
            $select->where($whereArr);
        }

        $output = $select->count();

        return $output;
    }    

}
