<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Library\Queryhelper;
use DB;

class NovelDetail extends Model {

    protected $table = 'noveldetail';
    protected $primaryKey = 'novelDetailID';

    const CREATED_AT = 'createdTime';
    const UPDATED_AT = 'updatedTime';

    public function novel() {
        return $this->belongsTo('App\Novel','novelID');
    }

    public function translator() {
        return $this->belongsTo('App\Translator','transID');
    }

    public function location() {
        return $this->belongsTo('App\BookLocation','novelLocation');
    }

}
