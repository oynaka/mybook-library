<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Library\Queryhelper;
use DB;

class OriginalPublish extends Model {

    protected $table = 'book_ori_publish';
    const CREATED_AT = NULL;
    const UPDATED_AT = NULL;

}
