<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Library\Queryhelper;
use DB;

class Translator extends Model {

    protected $table = 'book_translator';
    protected $primaryKey = 'transID';
    const CREATED_AT = NULL;
    const UPDATED_AT = NULL;

    public function mangaDetail() {
        return $this->hasMany('App\MangaDetail','transID','transID');
    }

    public function novelDetail() {
        return $this->hasMany('App\NovelDetail','transID','transID');
    }

    public function ceDetail() {
        return $this->hasMany('App\ComicEssayDetail','transID','transID');
    }

}
