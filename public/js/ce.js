$("#editModal").on("hidden.bs.modal", function() {
    $('#ce_info_frm').trigger("reset");
    $("div.text-danger").remove();
});

$("#date_buy").datepicker({
    dateFormat: "yy-mm-dd"
});

$("#detail_date_buy").datepicker({
    dateFormat: "yy-mm-dd"
});

$('#editDetailModal').on('show.bs.modal', function(e) {
    //alert('do this befor open');
    ceDetailID = $(e.relatedTarget).data('ce-id');
    $('#ceDetailID').val(ceDetailID);

    $.ajax({
        type: "GET",
        url: '/ajax/getCEDetail',
        dataType: "json",
        data: { ceDetailID: ceDetailID },
        success: function(msg) {
            $('#detail_location').val(msg.location);
            $('#detail_date_buy').val(msg.date_buy);
            $('#detail_translator').val(msg.transID);

            if (msg.readFlg == 1) {
                $('#detail_read_flg').prop('checked', true);
            } else {
                $('#detail_read_flg').prop('checked', false);
            }

        }
    });
})


$('#ce_info_frm').validate({ // initialize the plugin
    rules: {
        ceName: {
            required: true
        },
        publishID: {
            required: true
        }
    },
    messages: {
        mangaName: "Comic essay name is required",
        publishID: "Publish is required"
    },
    errorElement: "div",
    errorClass: "text-danger"
})