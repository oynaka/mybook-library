$('#editModal').on('show.bs.modal', function(e) {
    mangaID = $(e.relatedTarget).data('manga-id');

    $("#authorDiv").empty();
    $("#artistDiv").empty();
    $("#charDiv").empty();

    $.ajax({
        type: "GET",
        url: '/ajax/getMangaAuthor/' + mangaID,
        dataType: "json",
        success: function(msg) {

            var char_count = 0;
            $.each(msg, function(key, value) {

                element = createAuthorElement(value.role_id, value.author_id, value.authors.name);

                switch (value.role_id) {
                    case 1:
                        var div_name = 'authorDiv';
                        break;
                    case 2:
                        var div_name = 'artistDiv';
                        break;
                    case 3:
                        var div_name = 'charDiv';
                        char_count = char_count + 1;
                        break;
                }

                jQuery('#' + div_name).append(element);

            });

            if (char_count == 0) {
                addEmptyRow('charDiv');
            }
        }
    });

})

$("#editModal").on("hidden.bs.modal", function() {
    $('#manga_info_frm').trigger("reset");
    $("div.text-danger").remove();

    //-- reset author,artist , char design
    //$("#authorDiv").empty();

});

$('#remarkModal').on('show.bs.modal', function(e) {
    var manga_id = $(e.relatedTarget).data('manga-id');

    $.ajax({
        type: "GET",
        url: '/ajax/getMangaRemark',
        dataType: "json",
        data: { manga_id: manga_id },
        success: function(msg) {
            $('#showRemark').html(msg.remark.replace(/\n/g,"<br>"));
        }
    });
});

$("#date_buy").datepicker({
    dateFormat: "yy-mm-dd"
});

$("#detail_date_buy").datepicker({
    dateFormat: "yy-mm-dd"
});

$('#imageModal').on('show.bs.modal', function(e) {
    //alert('do this befor open');
    //mangaDetailID = e.relatedTarget.id;
    var image_path = $(e.relatedTarget).data('manga-id');
    //alert($(e.relatedTarget).data('manga-id'));

    $("#show_image").attr("src", image_path);

})

$('#editDetailModal').on('show.bs.modal', function(e) {
    //alert('do this befor open');
    //mangaDetailID = e.relatedTarget.id;
    mangaDetailID = $(e.relatedTarget).data('manga-id');
    $('#mangaDetailID').val(mangaDetailID);

    $.ajax({
        type: "GET",
        url: '/ajax/getMangaDetail',
        dataType: "json",
        data: { mangaDetailID: mangaDetailID },
        success: function(msg) {

            $('#detail_volumn').html(msg.mangaVol);
            $('#detail_location').val(msg.mangaLocation);
            $('#detail_date_buy').val(msg.mangaDateBuy);
            $('#detail_translator').val(msg.transID);
            $('#detail_price').val(msg.mangaPrice);
            $('#detail_quan').val(msg.mangaQuan);
            $('#detail_photo').val(msg.mangaPhoto);


            if (msg.readFlg == 1) {
                $('#detail_read_flg').prop('checked', true);
            } else {
                $('#detail_read_flg').prop('checked', false);
            }

        }
    });
})


$('#manga_info_frm').validate({ // initialize the plugin
    rules: {
        mangaName: {
            required: true
        },
        publishID: {
            required: true
        }
    },
    messages: {
        mangaName: "Manga name is required",
        publishID: "Publish is required"
    },
    errorElement: "div",
    errorClass: "text-danger"
})

$("#AddMoreAuthor").click(function() {

    if ($("#author_input").val() != "" && $("#author_id").val() != "") {
        var element = createAuthorElement(1, $("#author_id").val(), $("#author_input").val());
        jQuery('#authorDiv').append(element);
    }

});

$("#AddMoreArtist").click(function() {

    if ($("#artist_input").val() != "" && $("#artist_id").val() != "") {
        var element = createAuthorElement(2, $("#artist_id").val(), $("#artist_input").val());
        jQuery('#artistDiv').append(element);
    }

});

$("#AddMoreChar").click(function() {
    if ($("#char_input").val() != "" && $("#char_id").val() != "") {

        var count = $('#charDiv > .emptyRow').length;
        if (count > 0) {
            $('#charDiv > .emptyRow').remove();
        }

        var element = createAuthorElement(3, $("#char_id").val(), $("#char_input").val());
        jQuery('#charDiv').append(element);
    }

});


$("ul").on('click', 'li .delElement', function() {
    var count = $(this).closest('ul').children('li').length;
    if (count > 1) {
        $(this).closest('li').remove();
    }
});

$("ul").on('click', 'li .delCharElement', function() {
    $(this).closest('li').remove();
});

function createAuthorElement(role_id, author_id, author_name) {

    switch (role_id) {
        case 1:
            var hidden_name = "author_id_box[]";
            var delClass = "delElement";
            break;
        case 2:
            var hidden_name = "artist_id_box[]";
            var delClass = "delElement";
            break;
        case 3:
            var hidden_name = "char_id_box[]";
            var delClass = "delCharElement";
            break;
    }

    var inputVal = '<input type="hidden" name="' + hidden_name + '" value="' + author_id + '">';
    var textVal = author_name;
    var closeBtn = '<button type="button" class="close ' + delClass + '" aria-label="Close"><span aria-hidden="true">&times;</span></button>';

    var output = '<li class="list-group-item list-group-item-info">' + textVal + inputVal + closeBtn + '</li>';

    return output;
}

function addEmptyRow(div_name) {

    var element = '<li class="list-group-item list-group-item-default disabled emptyRow" style="text-align: center" >----- No charactor design ------</li>';

    jQuery('#' + div_name).append(element);
}
