$("#editModal").on("hidden.bs.modal", function() {
    $('#novel_info_frm').trigger("reset");
    $("div.text-danger").remove();
});

$("#date_buy").datepicker({
    dateFormat: "yy-mm-dd"
});

$("#detail_date_buy").datepicker({
    dateFormat: "yy-mm-dd"
});

$('#imageModal').on('show.bs.modal', function(e) {
    //alert('do this befor open');
    var image_path = $(e.relatedTarget).data('novel-id');
    $("#show_image").attr("src", image_path);
})

$('#editDetailModal').on('show.bs.modal', function(e) {
    //alert('do this befor open');
    //novelDetailID = e.relatedTarget.id;
    novelDetailID = $(e.relatedTarget).data('novel-id');
    $('#novelDetailID').val(novelDetailID);

    $.ajax({
        type: "GET",
        url: '/ajax/getNovelDetail',
        dataType: "json",
        data: { novelDetailID: novelDetailID },
        success: function(msg) {
            $('#detail_location').val(msg.novelLocation);
            $('#detail_date_buy').val(msg.novelDateBuy);
            $('#detail_translator').val(msg.transID);
            $('#detail_photo').val(msg.novelPhoto);

            if (msg.readFlg == 1) {
                $('#detail_read_flg').prop('checked', true);
            } else {
                $('#detail_read_flg').prop('checked', false);
            }

        }
    });
})


$('#novel_info_frm').validate({ // initialize the plugin
    rules: {
        mangaName: {
            required: true
        },
        publishID: {
            required: true
        }
    },
    messages: {
        mangaName: "Manga name is required",
        publishID: "Publish is required"
    },
    errorElement: "div",
    errorClass: "text-danger"
})