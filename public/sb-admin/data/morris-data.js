$(function() {

    $.ajax({
        'url': '/ajax/graph/setAreaGraph',
        'dataType': "json",
        'success': function(data) {
            setAreaGraph(data);
        }
    });

    $.ajax({
        'url': '/ajax/graph/setBarGraph',
        'dataType': "json",
        'success': function(data) {
            setBarGraph(data);
        }
    });

    $.ajax({
        'url': '/ajax/graph/setBarReadGraph',
        'dataType': "json",
        'success': function(data) {
            setBarReadGraph(data);
        }
    });

    $.ajax({
        'url': '/ajax/graph/setDonutGraph',
        'dataType': "json",
        'success': function(data) {
            setDonutGraph(data);
        }
    });

});

function changeAreaGraphYear(year) {
    $('#area_year_div').html(year);
    $('#morris-area-chart').empty();
    $.ajax({
        'url': '/ajax/graph/setAreaGraph/' + year,
        'dataType': "json",
        'success': function(data) {
            setAreaGraph(data);
        }
    });
}

function changeDonutGraphYear(year) {
    $('#donut_year_div').html(year);
    $('#morris-donut-chart').empty();
    $.ajax({
        'url': '/ajax/graph/setDonutGraph/' + year,
        'dataType': "json",
        'success': function(data) {
            setDonutGraph(data);
        }
    });
}

function setAreaGraph(data) {
    Morris.Area({
        element: 'morris-area-chart',
        data: data,
        xkey: 'period',
        ykeys: ['Manga', 'Novel', 'comicessay'],
        labels: ['Manga', 'Novel', 'Comic Essay'],
        pointSize: 2,
        hideHover: 'auto',
        behaveLikeLine: true,
        //gridTextColor:'#ff0000',
        fillOpacity: 0.5,
        resize: true,
        lineColors: ['#0B62A4', '#4DA74D', '#7A92A3']
    });
}

function setBarGraph(data) {
    Morris.Bar({
        element: 'morris-bar-chart',
        data: data,
        xkey: 'y',
        ykeys: ['Manga', 'Novel', 'comicessay'],
        labels: ['Manga', 'Novel', 'Comic Essay'],
        hideHover: 'auto',
        resize: true,
        barColors: ['#0B62A4', '#4DA74D', '#7A92A3']
    });
}

function setBarReadGraph(data) {
    Morris.Bar({
        element: 'morris-bar-read-chart',
        data: data,
        xkey: 'year',
        ykeys: ['value'],
        labels: ['value']
    });
}

function setDonutGraph(data) {
    Morris.Donut({
        element: 'morris-donut-chart',
        data: data,
        resize: true
    });
}