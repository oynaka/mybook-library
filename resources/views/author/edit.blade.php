@extends('layouts.master')
@section('content')
<div class="row">
        <div class="col-lg-12">
            <h3 class="page-header">Update Author</h3>
        </div>
        <!-- /.col-lg-12 -->
    </div>

    <div class="row">
        <div class="panel panel-default">
        <div class="panel-heading clearfix">

          @if (Session::has('flash_message'))
            @if(Session::get('flash_message') == 'updated')
              <div class="alert alert-success">
                <a class="close" data-dismiss="alert">×</a>
                <strong>Done!</strong> Updated success.
              </div>
            @elseif(Session::get('flash_message') == 'inserted')
              <div class="alert alert-success">
                <a class="close" data-dismiss="alert">×</a>
                <strong>Done!</strong> Inserted success.
              </div>
            @else
              <div class="alert alert-danger">
                <a class="close" data-dismiss="alert">×</a>
                  <strong>Error!</strong> Cannot update, please try again.
              </div>
            @endif
          @endif

          @if ($errors->any())
          <div class="alert alert-danger">
              <ul>
                  @foreach ($errors->all() as $error)
                      <li>{{ $error }}</li>
                  @endforeach
              </ul>
          </div>
          @endif

      <form action="" method="POST" >
        {{ csrf_field() }}
        <div class="form-group">
            <label for="name">Name</label>
            <input type="text" class="form-control" id="name" name="name" value="{{$author->name}}">
        </div>

        <div class="form-group">
          <label for="native_name">Native Name</label>
          <input type="text" class="form-control" id="native_name" name="native_name" value="{{$author->native_name}}">
        </div>

        <input type="hidden" name="mode" value="update">
        <button type="submit" class="btn btn-primary">Submit</button>
        <button class="btn btn-default" type="reset">Reset</button>
      </form>

        </div>
        </div>
    </div>
@endsection