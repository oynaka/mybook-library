@extends('layouts.master')
@section('content')
<div class="row">
    <div class="col-lg-12">
        <h3 class="page-header">Author</h3>
    </div>
    <!-- /.col-lg-12 -->
</div>

<div class="row">
      <div class="panel panel-default">
        <div class="panel-heading clearfix">
          <form class="navbar-form navbar-left" role="search">
            {{ csrf_field() }}
            <div class="form-group">
              <input type="text" id="search_name" name="search_name" class="form-control" placeholder="Search" value="{{$search_name}}">
            </div>
            <button type="submit" class="btn btn-default">Submit</button>
            <button type="button" class="btn btn-default" id="reset_search">Reset</button>
          </form>

          <div class="navbar-form navbar-right">
            <a  href="/author/insert" class="btn btn-info btn-sm" target="_blank">Add a new</a>
          </div>
        </div>
        <!-- panel-heading clearfix -->
      </div>
      <!-- panel panel-default -->

      <!-- Table -->
      <table class="table table-striped table-bordered table-condensed">
        <thead>
          <tr>
            <th>#</th>
            <th>Name</th>
            <th>Native Name</th>
            <th>Example work</th>
            <th>&nbsp;</th>
          </tr>
        </thead>
        <tbody>
            @if (isset($allAuthor))
                @foreach($allAuthor as $author)
                    <tr>
                        <td>
                          {{$loop->iteration + $allAuthor->firstItem()-1}}
                        </td>
                        <td>{{$author->name}}</td>
                        <td>{{$author->native_name}}</td>
                        <td>
                          <a href="/{{$author->sampleBookType}}/view/{{$author->sampleBookID}}" target="_blank">
                            {{$author->sampleBookName}}
                          </a>
                        </td>
                        <td>
                          <a href="/author/edit/{{$author->id}}" target="_blank" class="btn btn-success btn-xs" >Edit</a>
                          <a href="/manga?author_id={{$author->id}}" target="_blank" class="btn btn-info btn-xs" >View</a>
                        </td>
                    </tr>
                @endforeach
            @endif
        </tbody>
      </table>
      <nav style="text-align:center">{{$allAuthor->appends(request()->except('_token'))->links()}}</nav>
</div> <!-- row -- >
@endsection
