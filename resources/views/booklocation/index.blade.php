@extends('layouts.master')
@section('content')
<div class="row">
    <div class="col-lg-12">
        <h3 class="page-header">Book Location</h3>
    </div>
    <!-- /.col-lg-12 -->
</div>

<div class="row">
      <div class="panel panel-default">
        <div class="panel-heading clearfix">
          <form class="navbar-form navbar-left" role="search">
            {{ csrf_field() }}
            <div class="form-group">
              <input type="text" name="search_name" class="form-control" placeholder="Search" value="{{$search_name}}">
            </div>
            <button type="submit" class="btn btn-default">Submit</button>
          </form>

          <div class="navbar-form navbar-right">
            <a  href="/booklocation/insert" class="btn btn-info btn-sm" target="_blank">Add a new</a>
          </div>
        </div>
        <!-- panel-heading clearfix -->
      </div>
      <!-- panel panel-default -->

      <!-- Table -->
      <table class="table table-striped table-bordered table-condensed">
        <thead>
          <tr>
            <th>#</th>
            <th>Location</th>
            <th>Book Quan.</th>
            <th>&nbsp;</th>
          </tr>
        </thead>
        <tbody>
            @if (isset($book_location_list))
                @foreach($book_location_list as $book_location)
                    <tr>
                        <td>{{$loop->iteration + $book_location_list->firstItem()-1}}</td>
                        <td>{{$book_location->location}}</td>
                        <td>{{$book_location->manga_detail_count + $book_location->novel_detail_count}}</td>
                        <td>
                          <a href="/booklocation/view/{{$book_location->id}}" target="_blank" class="btn btn-info btn-xs" >View</a>
                          <a href="/booklocation/edit/{{$book_location->id}}" target="_blank" class="btn btn-success btn-xs" >Edit</a>
                        </td>
                    </tr>
                @endforeach
            @endif
        </tbody>
      </table>
      <nav style="text-align:center">{{$book_location_list->appends(request()->except('_token'))->links()}}</nav>
</div> <!-- row -- >
@endsection