@extends('layouts.master')
@section('content')
<div class="row">
    <div class="col-lg-12">
        <h3 class="page-header">Original Publish</h3>
    </div>
    <!-- /.col-lg-12 -->
</div>

<div class="row">
      <div class="panel panel-default">
        <div class="panel-heading clearfix">
          <form class="navbar-form navbar-left" role="search">
            {{ csrf_field() }}
            <div class="form-group">
              <input type="text" name="search_name" class="form-control" placeholder="Search" value="{{$search_name}}">
            </div>
            <button type="submit" class="btn btn-default">Submit</button>
          </form>

          <div class="navbar-form navbar-right">
            <a  href="/oripublish/insert" class="btn btn-info btn-sm" target="_blank">Add a new</a>
          </div>
        </div>
        <!-- panel-heading clearfix -->
      </div>
      <!-- panel panel-default -->

      <!-- Table -->
      <table class="table table-striped table-bordered table-condensed">
        <thead>
          <tr>
            <th colspan="1" rowspan="2" class="text-center"><strong>No.</strong></th>
            <th colspan="1" rowspan="2" class="text-center"><strong>Name</strong></th>
            <th colspan="2" rowspan="1" class="text-center"><strong>Manga</strong></th>
            <th colspan="2" rowspan="1" class="text-center"><strong>Novel</strong></th>
            <th colspan="1" rowspan="2" class="text-center"><strong>Commic essey</strong></th>
            <th colspan="1" rowspan="2">&nbsp;</td>
          </tr>
          <tr>
            <th class="text-center"><strong>Series</strong></th>
            <th class="text-center"><strong>Books</strong></th>
            <th class="text-center"><strong>Series</strong></th>
            <th class="text-center"><strong>Books</strong></th>
          </tr>
        </thead>

        <tbody>
          @if (isset($allArr))
            @foreach($allArr as $item)
              <tr>
                <td class="text-center">
                  {{$loop->iteration + $allArr->firstItem()-1}}
                </td>
                <td>{{$item->name}}</td>
                <td class="text-center">{{$item->mangaSeriesCount}}</td>
                <td class="text-center">{{$item->mangaBooksCount}}</td>
                <td class="text-center">-</td>
                <td class="text-center">-</td>
                <td class="text-center">-</td>
                <td>
                  <a href="/oripublish/view/{{$item->id}}" target="_blank" class="btn btn-info btn-xs" >View</a>
                  <a href="/oripublish/edit/{{$item->id}}" target="_blank" class="btn btn-success btn-xs" >Edit</a>
                </td>
              </tr>
            @endforeach
          @endif
        </tbody>
      </table>
      <nav style="text-align:center">{{$allArr->appends(request()->except('_token'))->links()}}</nav>
</div> <!-- row -- >
@endsection