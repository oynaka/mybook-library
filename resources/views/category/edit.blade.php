@extends('layouts.master')
@section('content')
  <div class="row">
    <div class="col-lg-12">
        <h3 class="page-header">Update Category</h3>
    </div>
    <!-- /.col-lg-12 -->
  </div>

  <div class="row">
    <div class="panel panel-default">
    <div class="panel-heading clearfix">

      @if (Session::has('flash_message'))
        @if(Session::get('flash_message') == 'updated')
          <div class="alert alert-success">
            <a class="close" data-dismiss="alert">×</a>
            <strong>Done!</strong> Updated success.
          </div>
        @elseif(Session::get('flash_message') == 'inserted')
          <div class="alert alert-success">
            <a class="close" data-dismiss="alert">×</a>
            <strong>Done!</strong> Inserted success.
          </div>
        @else
          <div class="alert alert-danger">
            <a class="close" data-dismiss="alert">×</a>
              <strong>Error!</strong> Cannot update, please try again.
          </div>
        @endif
      @endif

      @if ($errors->any())
      <div class="alert alert-danger">
          <ul>
              @foreach ($errors->all() as $error)
                  <li>{{ $error }}</li>
              @endforeach
          </ul>
      </div>
      @endif

      <form name="editInfoFrm" action="" method="POST" >
        {{ csrf_field() }}
        <div class="form-group">
          <label for="category_name">Name</label>
          <input type="text" class="form-control" id="catName" name="catName" value="{{$categoryInfo->catName}}">
        </div>

        <div class="form-group">
          <label for="remark">Remark</label>
          <textarea class="form-control" id="catRemark" name="catRemark">{{$categoryInfo->catRemark}}</textarea>
        </div>

        <input type="hidden" name="mode" value="update">
        <button type="submit" class="btn btn-primary">Submit</button>
        <button class="btn btn-default" type="reset">Reset</button>
      </form>

    </div>
    </div>
  </div>

  <div class="row">
    <div class="panel panel-info">
    <!-- Default panel contents -->
      <div class="panel-heading">Manga in this category</div>

      <!-- Table -->
      <table class="table table-striped table-bordered table-condensed">
        <thead>
        <tr>
          <th style="text-align:center;">#</th>
          <th>Book Name</th>
          <th>Book Native Name</th>
          <th>&nbsp;</th>
        </tr>
        </thead>
        <tbody>
          @if (isset($mangaList) && count($mangaList)>0)
            @foreach($mangaList as $item)
              <tr>
                <td class="text-center">{{$loop->iteration}}</td>
                <td>{{$item->manga->mangaName}}</td>
                <td>{{$item->manga->mangaNameEng}}</td>
                <td><a href="/manga/view/{{$item->manga->mangaID}}" class="btn btn-info btn-xs" target="_blank" >view</a></td>
              </tr>
            @endforeach
          @else
            <tr><td colspan="5">&nbsp;</td></tr>
          @endif
        </tbody>
      </table>
    </div>
  </div>

@endsection