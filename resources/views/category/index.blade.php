@extends('layouts.master')
@section('content')
<div class="row">
    <div class="col-lg-12">
        <h3 class="page-header">Category</h3>
    </div>
    <!-- /.col-lg-12 -->
</div>

<div class="row">
      <div class="panel panel-default">
        <div class="panel-heading clearfix">
          <form class="navbar-form navbar-left" role="search">
            {{ csrf_field() }}
            <div class="form-group">
              <input type="text" name="search_name" class="form-control" placeholder="Search" value="{{$search_name}}">
            </div>
            <button type="submit" class="btn btn-default">Submit</button>
          </form>

          <div class="navbar-form navbar-right">
            <a  href="/category/insert" class="btn btn-info btn-sm" target="_blank">Add a new</a>
          </div>
        </div>
        <!-- panel-heading clearfix -->
      </div>
      <!-- panel panel-default -->

      <!-- Table -->
      <table class="table table-striped table-bordered table-condensed">
        <thead>
          <tr>
            <th>#</th>
            <th>Name</th>
            <th>&nbsp;</th>
          </tr>
        </thead>
        <tbody>
            @if (isset($category_list))
                @foreach($category_list as $category)
                    <tr>
                        <td>{{$loop->iteration + $category_list->firstItem()-1}}</td>
                        <td>{{$category->catName}}</td>
                        <td>
                          <a href="/category/edit/{{$category->catID}}" target="_blank" class="btn btn-info btn-xs" >View & Edit</a>
                        </td>
                    </tr>
                @endforeach
            @endif
        </tbody>
      </table>
      <nav style="text-align:center">{{$category_list->appends(request()->except('_token'))->links()}}</nav>
</div> <!-- row -- >
@endsection