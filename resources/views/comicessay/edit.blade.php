@if ($errors->any())
<div class="alert alert-danger">
    <ul>
        @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
        @endforeach
    </ul>
</div>
@endif
<div class="panel panel-primary">
    <div class="panel-heading">Information</div>
    <form id="ce_info_frm" action="" method="POST">
      {{ csrf_field() }}
      <table class="table table-bordered" >
        <tr>
          <td class="active" width="30%">ID</td>
          <td>{{$bookInfo->ceID}}</td>
        </tr>
        <tr>
          <td class="active">Name</td>
          <td>
            <input class="form-control" type="text" name="ceName" id="ceName" value="{{$bookInfo->ceName}}">
          </td>
        </tr>
        <tr>
          <td class="active">Name Eng</td>
          <td><input class="form-control" type="text" name="ceNameEng" id="ceNameEng" value="{{$bookInfo->ceNameEng}}"></td>
        </tr>
        <tr>
          <td class="active">Native Name</td>
          <td><input class="form-control" type="text" name="ceNativeName" id="ceNativeName" value="{{$bookInfo->native_name}}"></td>
        </tr>

        <tr>
          <td class="active">Author</td>
          <td>
            <input
              class="form-control bs-autocomplete"
              id="ac-demo"
              value="{{$authors[0]->authors->name}}"
              placeholder=""
              type="text"
              data-source="/ajax/callAuthor"
              data-hidden_field_id="author_id"
              data-item_id="id"
              data-item_label="authorName"
              autocomplete="off"
            >
            <input class="form-control" id="author_id" name="author_id" value="{{$authors[0]->author_id}}" type="hidden" >
          </td>
        </tr>

        <tr>
          <td class="active">Publish</td>
          <td>
            <select class="form-control" name="publishID" id="publishID">
              <option value="">----------------</option>
              @foreach($allPublish as $publish)
                <option value="{{$publish->id}}" 
                  {{ $bookInfo->publishID == $publish->id ? 'selected="selected"' : '' }}
                >
                  {{$publish->name}}
                </option>
              @endforeach
            </select>
          </td>
        </tr>

        <tr>
          <td class="active">Status</td>
          <td>
            <select class="form-control" name="status" id="status">
              @foreach($allStatus as $status)
                <option value="{{$status['id']}}" 
                  {{ $bookInfo->status == $status['id'] ? 'selected="selected"' : '' }}
                >
                  {{$status['name']}}
                </option>
              @endforeach
            </select>
          </td>
        </tr>

        <tr>
          <td colspan="2" align="center">
            <input type="hidden" value="edit" name="mode">
            <button class="btn btn-primary btn-sm" type="submit" >Save</button>
            <button class="btn btn-primary btn-sm" type="button" data-dismiss="modal" >Close</button>
          </td>
        </tr>
      </table>
    </form>
</div>