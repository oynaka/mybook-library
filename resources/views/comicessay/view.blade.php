@extends('layouts.master')
@section('content')
<div class="row">
  <div class="col-lg-12">
      <h3 class="page-header">View Comic Essay</h3>
  </div>
  <!-- /.col-lg-12 -->
</div>

<div class="row">
  
@if (Session::has('flash_message'))
  @if(Session::get('flash_message') == 'updated')
    <div class="alert alert-success">
      <a class="close" data-dismiss="alert">×</a>
      <strong>Well done!</strong> update Comic Essay info success.
    </div>
  @endif

  @if(Session::get('flash_message') == 'new_vol_inserted')
    <div class="alert alert-success">
      <a class="close" data-dismiss="alert">×</a>
      <strong>Well done!</strong> insert Comic Essay new volumn success.
    </div>
  @endif

  @if(Session::get('flash_message') == 'all_loc_updated')
    <div class="alert alert-success">
      <a class="close" data-dismiss="alert">×</a>
      <strong>Well done!</strong> all location update success.
    </div>
  @endif  

  @if(Session::get('flash_message') == 'detail_updated')
    <div class="alert alert-success">
      <a class="close" data-dismiss="alert">×</a>
      <strong>Well done!</strong> update Comic Essay detail success.
    </div>
  @endif  

@endif

<div class="col-lg-6">
  <div class="panel panel-primary">
    <div class="panel-heading">Information</div>
        <table class="table table-bordered" >
          <tr>
            <td class="active" width="30%">ID</td>
            <td>{{$bookInfo->ceID}}</td>
          </tr>
          <tr>
            <td class="active">Name</td>
            <td>{{$bookInfo->ceName}}</td>
          </tr>
          <tr>
            <td class="active">Name Eng</td>
            <td>{{$bookInfo->ceNameEng}}</td>
          </tr>
          <tr>
            <td class="active">Native Name</td>
            <td>{{$bookInfo->native_name}}</td>
          </tr>

          <tr>
            <td class="active">Author</td>
            <td>
              @foreach($authors as $author)
                {{$author->authors->name}}<br/>
              @endforeach
            </td>
          </tr>

          <tr>
            <td class="active">Publish</td>
            <td>{{$publish->name}}</td>
          </tr>

          <tr>
            <td class="active">Status</td>
            <td>{{$status}}</td>
          </tr>

          <tr>
            <td colspan="2" align="center">
              <button class="btn btn-primary btn-sm" type="button" data-toggle="modal" data-target="#editModal" data-whatever="">Edit</button>
            </td>
          </tr>
        </table>
    </div>
</div>


@if($bookInfo->status != 'C')
<div class="col-lg-6">
  <form id="ce_new_vol_frm" action="" method="POST">
    {{ csrf_field() }}
    <div class="panel panel-info">
      <div class="panel-heading">New volumn</div>
        <table class="table table-bordered" >
          <tr>
            <td>Vol.</td>
            <td><input class="form-control" type="text" name="vol" id="vol" value="{{$nextVol}}"></td>
          </tr>
          <tr>
            <td>Price</td>
            <td><input class="form-control" type="text" name="price" id="price" value="{{$latestPrice}}"></td>
          </tr>
          <tr>
            <td>Date Buy</td>
            <td>
              <input class="form-control" type="text" name="date_buy" id="date_buy" value="{{$currentDate}}">
            </td>
          </tr>
          <tr>
            <td colspan="2" align="center">
              <input type="hidden" name="mode" value="newVol">
              <button class="btn btn-info btn-sm" id="save_new_vol" type="submit" data-ce-id="">Save</button>
            </td>
          </tr>
        </table>
    </div>  <!-- panel panel-info -->
  </form>
</div> <!-- col-lg-6 -->
@endif

<div class="col-lg-6">
  <div class="panel panel-info">
    <div class="panel-heading">Update all location</div>
    <div class="panel-body">
      <form id="all_location_frm" action="" method="POST">
        {{ csrf_field() }}
        <div class="form-group">
          <select class="form-control" name="all_location">
            <option value="">-----Select book location-----</option>
            @foreach($allLocation as $location)
              <option value="{{$location->id}}">
                {{$location->location}}
              </option>
            @endforeach
          </select>
        </div>
        <input type="hidden" name="mode" value="allLoc">
        <button type="submit" class="btn btn-primary" data-ce-id="" id="update_all_location">Update all seies</button>
      </form>
    </div>
  </div>
</div>

</div> <!-- row -->



<div class="row">
<div class="panel panel-info">

  <div class="panel-heading">
    <div class="nav">
      <div class="pull-left">Volumn detail</div>
      <div class="pull-right">
        <button type="button" class="btn btn-primary btn-xs" data-toggle="modal" data-target="#allImageModal" data-whatever="{{$bookInfo->ceID}}"><i class="glyphicon glyphicon-picture" ></i> All Photo</button>
      </div>
    </div>
  </div>

  <div class="panel-body">

    @foreach($bookDetail as $detail)
    <div class="col-md-3">

     <div class="thumbnail clearfix" id="thumb_{{$detail->vol}}">

        <div class="form-group">
          <span class="ce-volume">#{{$detail->vol}} </span>
          @if($detail->readFlg == 1)
            <span class="label label-success">Read</span>
          @else
            <span class="label label-default">Unread</span>
          @endif

          <button type="button" class="close pull-right" data-toggle="modal" data-target="#deleteModal" data-whatever="">&times;</button>
        </div>

        <div class="form-group">
          <label>Location :</label>
          <span>{{$allLocation->find($detail->location)['location']}}</span>
        </div>

        <div class="form-group">
          <label>Date :</label>
          <span>
            {{$detail->date_buy}}
          </span>
        </div>
        <div class="form-group">
          <label>Translator :</label>
          <span>{{$allTranslator->find($detail->transID)['translator_name']}}</span>
        </div>

        @if($detail->photo != '')
        <button type="button" class="btn btn-default btn-xs pull-left" data-toggle="modal" data-target="#imageModal" data-whatever=""><i class="glyphicon glyphicon-picture" ></i> Photo</button>
        @endif

        <button type="button" class="btn btn-info btn-xs pull-right" data-toggle="modal" data-target="#editDetailModal" data-ce-id="{{$detail->ce_detail_id}}">
          <i class="glyphicon glyphicon-pencil"></i> Edit
        </button>

      </div>
      <!-- thumbnail -->

    </div>
    <!-- col-md-4 -->
    @endforeach

  </div>
  <!-- panel-body -->
</div>
<!-- panel panel-info -->
</div>
<!-- row -->

<!-- edit form -->
<div class="modal fade" id="editModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title" id="myModalLabel">Edit comic essay info</h4>
        </div>
        <div class="modal-body">
          @include('comicessay.edit')
        </div>
        <div class="modal-footer">
          &nbsp;
        </div>
      </div>
  </div>
</div>
<!-- edit form -->

<!-- edit form -->
<div class="modal fade" id="editDetailModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title" id="myModalLabel">Edit comic essay detail</h4>
        </div>
        <div class="modal-body">
          @include('comicessay.editdetail')
        </div>
        <div class="modal-footer">
          &nbsp;
        </div>
      </div>
  </div>
</div>
<!-- edit form -->

<div class="modal fade" id="deleteModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
<div class="modal-dialog modal-sm">
  <div class="modal-content">
    <div class="modal-header">
      <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
      <h4 class="modal-title" id="myModalLabel">Delete Volumn</h4>
    </div>
    <div class="modal-body">
      Do want to delete this item ?
      <input type="hidden" value="" id="hidden_ce_detail_id" name="hidden_ce_detail_id" />
    </div>
    <div class="modal-footer">
      <button type="button" class="btn btn-default btn-xs" data-dismiss="modal">
        <i class="glyphicon glyphicon-remove"></i> Cancel
      </button>
      <button type="button" class="btn btn-danger btn-xs" id="deleteVol" data-dismiss="">
        <i class="glyphicon glyphicon glyphicon-remove" ></i> Delete
      </button>
    </div>
  </div>
</div>
</div>

<div class="modal fade" id="imageModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-sm">
    <div class="modal-content">
      <div class="modal-body">
        <center><img src="" id="show_image"></center>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="allImageModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
<div class="modal-dialog modal-lg">
  <div class="modal-content">
    <div class="modal-body">
<div class="row">
    <?php
      //echo '<pre>';print_r($manga);echo '</pre>';
      //foreach($mangaDetail as $key=>$value) {
    ?>
    <div class="col-md-3">

     <div class="thumbnail clearfix">
        <span class="manga-volume">#<?php //echo $value['mangaVol'] ?> </span>
        show image here
      </div>
      <!-- thumbnail -->

    </div>
    <!-- col-md-3 -->
    <?php
    //  }
    ?>
</div>



    </div>
  </div>
</div>
</div>


@endsection

@section('pagescript')
<script src="{{asset('jquery-ui/jquery-ui.min.js')}}"></script>
<script src="{{asset('js/jquery.validate.min.js')}}"></script>
<script src="{{asset('js/ce.js')}}"></script>
<script src="{{asset('js/call-autocomplete.js')}}"></script>
@endsection


@push('styles')
  <link href="{{asset('jquery-ui/jquery-ui.min.css')}}" rel="stylesheet" type="text/css">
  <link href="{{asset('css/ce.css')}}" rel="stylesheet" type="text/css">
@endpush