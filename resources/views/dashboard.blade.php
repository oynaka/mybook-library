@extends('layouts.master')
@section('content')
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">Dashboard</h1>
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->
    <div class="row">
        <div class="col-lg-6 col-md-6">
            <div class="panel panel-primary">
                <div class="panel-heading">
                    <div class="row">
                        <div class="col-xs-3">
                            <i class="fa fa-comments fa-5x"></i>
                        </div>
                        <div class="col-xs-9 text-right">
                            <div class="huge">{{$unread_book}}</div>
                            <div>Unread books</div>
                        </div>
                    </div>
                </div>
                <a href="/unread">
                    <div class="panel-footer">
                        <span class="pull-left">View Details</span>
                        <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                        <div class="clearfix"></div>
                    </div>
                </a>
            </div>
        </div>

        <div class="col-lg-6 col-md-6">
            <div class="panel panel-green">
                <div class="panel-heading">
                    <div class="row">
                        <div class="col-xs-3">
                            <i class="fa fa-tasks fa-5x"></i>
                        </div>
                        <div class="col-xs-9 text-right">
                            <div class="huge">{{$new_book}}</div>
                            <div>New Books</div>
                        </div>
                    </div>
                </div>
                <a href="/newbook">
                    <div class="panel-footer">
                        <span class="pull-left">View Details</span>
                        <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                        <div class="clearfix"></div>
                    </div>
                </a>
            </div>
        </div>



    </div>
    <!-- /.row -->
    <div class="row">
        <div class="col-lg-8">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <i class="fa fa-bar-chart-o fa-fw"></i> Book year chart
                    <div id="area_year_div" style="display:inline;">{{date('Y')}}</div>
                    <div class="pull-right">
                        <div class="btn-group">
                            <button type="button" class="btn btn-default btn-xs dropdown-toggle" data-toggle="dropdown">
                                Year
                                <span class="caret"></span>
                            </button>
                            <ul class="dropdown-menu pull-right" role="menu">
                                @for ($i = date('Y'); $i >= 2008; $i--)
                                <li>
                                    <a href="javascript:;" onclick="changeAreaGraphYear({{$i}});">{{$i}}</a>
                                </li>
                                @endfor
                            </ul>
                        </div>
                    </div>
                </div>
                <!-- /.panel-heading -->
                <div class="panel-body">
                    <div id="morris-area-chart"></div>
                </div>
                <!-- /.panel-body -->
            </div>
            <!-- /.panel -->
            <div class="panel panel-default">
                <div class="panel-heading">
                    <i class="fa fa-bar-chart-o fa-fw"></i> Bar Chart
                </div>
                <!-- /.panel-heading -->
                <div class="panel-body">
                    <div class="row">
                        <div class="col-lg-12">
                            <div id="morris-bar-chart"></div>
                        </div>
                    </div>
                    <!-- /.row -->
                </div>
                <!-- /.panel-body -->
            </div>
            <!-- /.panel -->
            <div class="panel panel-default">
                <div class="panel-heading">
                    <i class="fa fa-bar-chart-o fa-fw"></i> Read Book chart
                </div>
                <!-- /.panel-heading -->
                <div class="panel-body">
                    <div id="morris-bar-read-chart"></div>
                </div>
                <!-- /.panel-body -->
            </div>
        </div>
        <!-- /.col-lg-8 -->
        <div class="col-lg-4">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <i class="fa fa-bell fa-fw"></i> Summary Panel
                </div>
                <!-- /.panel-heading -->
                <div class="panel-body">
                        <ul class="list-group">
                          <li class="list-group-item list-group-item-info">
                            <span class="badge">{{number_format($all_book,0)}}</span>
                            <strong>All book</strong>
                          </li>
                          <li class="list-group-item">
                            <span class="badge">{{number_format($all_manga,0)}}</span>
                            All manga
                          </li>
                          <li class="list-group-item">
                            <span class="badge">{{number_format($all_novel,0)}}</span>
                            All Novel
                          </li>
                          <li class="list-group-item">
                            <span class="badge">{{number_format($all_ce,0)}}</span>
                            All Comic Essay
                          </li>
                        </ul>
                        <!-- /.list-group -->
                    </div>
                <!-- /.panel-body -->
            </div>
            <!-- /.panel -->
            <div class="panel panel-default">
                <div class="panel-heading">
                    <i class="fa fa-bar-chart-o fa-fw"></i> All publish manga in year <div id="donut_year_div" style="display:inline;">{{date('Y')}}</div>
                    <div class="pull-right">
                        <div class="btn-group">
                            <button type="button" class="btn btn-default btn-xs dropdown-toggle" data-toggle="dropdown">
                                Year
                                <span class="caret"></span>
                            </button>
                            <ul class="dropdown-menu pull-right" role="menu">
                                @for ($i = date('Y'); $i >= 2008; $i--)
                                <li>
                                    <a href="javascript:;" onclick="changeDonutGraphYear({{$i}});">{{$i}}</a>
                                </li>
                                @endfor
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="panel-body">
                    <div id="morris-donut-chart"></div>
                </div>
                <!-- /.panel-body -->
            </div>
            <!-- /.panel -->
        </div>
        <!-- /.col-lg-4 -->
    </div>
    <!-- /.row -->
@endsection

@section('pagescript')
<script src="{{asset('sb-admin/data/morris-data.js')}}"></script>
@endsection