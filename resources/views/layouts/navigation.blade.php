<nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
    <div class="navbar-header">
        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
        </button>
        <a class="navbar-brand" href="/">PBMS v3.0</a>
    </div>
    <!-- /.navbar-header -->

    <ul class="nav navbar-top-links navbar-right">
        <li class="dropdown">
            <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                <i class="fa fa-user fa-fw"></i> <i class="fa fa-caret-down"></i>
            </a>
            <ul class="dropdown-menu dropdown-user">
                <li><a href="/logout"><i class="fa fa-sign-out fa-fw"></i> Logout</a>
                <li class="divider"></li>
                </li>
            </ul>
            <!-- /.dropdown-user -->
        </li>
        <!-- /.dropdown -->
    </ul>
    <!-- /.navbar-top-links -->

    <div class="navbar-default sidebar" role="navigation">
        <div class="sidebar-nav navbar-collapse">
            <ul class="nav" id="side-menu">
                <li>
                    <a href="/dashboard"><i class="fa fa-dashboard fa-fw"></i> Dashboard</a>
                </li>

                <li>
                    <a href="#"><i class="fa fa-wrench fa-fw"></i> Config<span class="fa arrow"></span></a>
                    <ul class="nav nav-second-level">
                        <li>
                            <a href="/booklocation">Locations</a>
                        </li>
                        <li>
                            <a href="/publish">Publish</a>
                        </li>
                        <li>
                            <a href="/oripublish">Original Publisher</a>
                        </li>
                        <li>
                            <a href="/category">Category</a>
                        </li>
                        <li>
                            <a href="/clear-data" target="_blank">Clear Data</a>
                        </li>
                    </ul>
                    <!-- /.nav-second-level -->
                </li>

                <li>
                    <a href="/author"><i class="fa fa-pencil fa-fw"></i> Author</a>
                </li>

                <li>
                    <a href="/translator"><i class="fa fa-pencil fa-fw"></i> Translator</a>
                </li>

                <li>
                    <a href="/manga"><i class="fa fa-book fa-fw"></i> Manga</a>
                </li>

                <li>
                    <a href="/novel"><i class="fa fa-book fa-fw"></i> Novel</a>
                </li>

                <li>
                    <a href="/comicessay"><i class="fa fa-book fa-fw"></i> Comic Essay</a>
                </li>

                <li>
                    <a href="#"><i class="fa fa-desktop fa-fw"></i> Media<span class="fa arrow"></span></a>
                    <ul class="nav nav-second-level">
                        <li>
                            <a href="/media">Media List</a>
                        </li>
                    </ul>
                    <!-- /.nav-second-level -->
                </li>
            </ul>
        </div>
        <!-- /.sidebar-collapse -->
    </div>
    <!-- /.navbar-static-side -->
    <!-- /.navbar-static-side -->
</nav>