@if ($errors->any())
<div class="alert alert-danger">
    <ul>
        @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
        @endforeach
    </ul>
</div>
@endif
<div class="panel panel-primary">
    <div class="panel-heading">Information</div>
    <form id="manga_info_frm" action="" method="POST">
      {{ csrf_field() }}
      <table class="table table-bordered" >
        <tr>
          <td class="active" width="30%">ID</td>
          <td>{{$mangaInfo->mangaID}}</td>
        </tr>
        <tr>
          <td class="active">Name</td>
          <td>
            <input class="form-control" type="text" name="mangaName" id="mangaName" value="{{$mangaInfo->mangaName}}">
          </td>
        </tr>
        <tr>
          <td class="active">Name Eng</td>
          <td><input class="form-control" type="text" name="mangaNameEng" id="mangaNameEng" value="{{$mangaInfo->mangaNameEng}}"></td>
        </tr>
        <tr>
          <td class="active">Native Name</td>
          <td><input class="form-control" type="text" name="mangaNativeName" id="mangaNativeName" value="{{$mangaInfo->mangaNativeName}}"></td>
        </tr>

        <tr>
          <td class="active">Author</td>
          <td>
            <input id="author_id" name="author_id" value="" type="hidden" >
            <div class="input-group">
                <input
                class="form-control bs-autocomplete selector"
                id="author_input"
                value=""
                placeholder=""
                type="text"
                data-source="/ajax/callAuthor"
                data-hidden_field_id="author_id"
                data-item_id="id"
                data-item_label="authorName"
                autocomplete="off"
                >

                <div class="input-group-btn">
                    <button class="btn btn-default" id="AddMoreAuthor" type="button">Add more author</button>
                </div><!-- /btn-group -->
            </div>
            <br/>
            <ul class="list-group" id="authorDiv"></ul>
          </td>
        </tr>

        <tr>
          <td class="active">Artist</td>
          <td>
            <div class="input-group">
              <input
                class="form-control bs-autocomplete"
                id="artist_input"
                value=""
                placeholder=""
                type="text"
                data-source="/ajax/callAuthor"
                data-hidden_field_id="artist_id"
                data-item_id="id"
                data-item_label="authorName"
                autocomplete="off"
              >

              <div class="input-group-btn">
                  <button class="btn btn-default" id="AddMoreArtist" type="button">Add more artist</button>
              </div><!-- /btn-group -->
            </div>
            <input id="artist_id" name="artist_id" value="" type="hidden" >

          <br/>
          <ul class="list-group" id="artistDiv"></ul>
          </td>
        </tr>

        <tr>
          <td class="active">Charactor Design</td>
          <td>
            <div class="input-group">
                <input
                  class="form-control bs-autocomplete"
                  id="char_input"
                  value=""
                  placeholder=""
                  type="text"
                  data-source="/ajax/callAuthor"
                  data-hidden_field_id="char_id"
                  data-item_id="id"
                  data-item_label="authorName"
                  autocomplete="off"
                >

                <div class="input-group-btn">
                    <button class="btn btn-default" id="AddMoreChar" type="button">Add more charactor design</button>
                </div><!-- /btn-group -->
              </div>
              <input id="char_id" value="" type="hidden" >

              <br/>
              <ul class="list-group" id="charDiv"></ul>
          </td>
        </tr>

        <tr>
          <td class="active">Publish</td>
          <td>
            <select class="form-control" name="publishID" id="publishID">
              <option value="">----------------</option>
              @foreach($allPublish as $publish)
                <option value="{{$publish->id}}"
                  {{ $mangaInfo->publishID == $publish->id ? 'selected="selected"' : '' }}
                >
                  {{$publish->name}}
                </option>
              @endforeach
            </select>
          </td>
        </tr>

        <tr>
          <td class="active">Original Publisher</td>
          <td>
            <select class="form-control" name="oriPublishID" id="oriPublishID">

              <option value="">----------------</option>
              @foreach($allOriPublish as $oriPublish)
                <option value="{{$oriPublish->id}}"
                  {{ $mangaInfo->oriPublishID == $oriPublish->id ? 'selected="selected"' : '' }}
                >
                  {{$oriPublish->name}}
                </option>
              @endforeach
            </select>
          </td>
        </tr>

        <tr>
          <td class="active">Status</td>
          <td>
            <select class="form-control" name="mangaStatus" id="mangaStatus">
              @foreach($allStatus as $status)
                <option value="{{$status['id']}}"
                  {{ $mangaInfo->mangaStatus == $status['id'] ? 'selected="selected"' : '' }}
                >
                  {{$status['name']}}
                </option>
              @endforeach
            </select>
          </td>
        </tr>

        <tr>
          <td class="active">External web</td>
          <td>
            <input class="form-control" type="text" name="externalWebID" id="externalWebID" value="{{$mangaInfo->externalWebID}}">
          </td>
        </tr>

          <tr>
              <td class="active">Remark</td>
              <td>
                <textarea
                        rows = "5"
                        class = "form-control"
                        name = "remark"
                        id = "remark"
                >{{$mangaInfo->remark}}</textarea>
              </td>
          </tr>

        <tr>
          <td class="active">Category</td>
          <td>
            <div class="container-fluid">
            <div class="row">
                @foreach($allCategory as $key => $category)
                @if(($key % 2) == 0  && $key >0 )
                <div class="clearfix"></div>
                @endif
                <div class="col-md-6">
                    <div class="checkbox">
                        <label>
                            <input type="checkbox"
                                  value="{{$category->catID}}"
                                  name="manga_category[]"
                                  @if($bookCategory->contains('catID',$category->catID))
                                    checked
                                  @endif
                            >
                            {{$category->catName}}
                          </label>
                    </div>
                </div>
                @endforeach
            </div>
            </div>
          </td>
        </tr>

        <tr>
          <td colspan="2" align="center">
            <input type="hidden" value="edit" name="mode">
            <button class="btn btn-primary btn-sm" type="submit" id="save_mangaInfo"  >Save</button>
            <button class="btn btn-primary btn-sm" type="button" id="close_mangaInfo" data-dismiss="modal" >Close</button>
          </td>
        </tr>
      </table>
    </form>
</div>