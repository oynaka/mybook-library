<div class="panel panel-primary">
    <div class="panel-heading">Detail</div>
    <form id="manga_detail_frm" action="" method="POST">
      {{ csrf_field() }}
      <table class="table table-bordered" >
        <tr>
          <td class="active" width="30%">Location</td>
          <td>
            <select class="form-control" name="detail_location" id="detail_location">
              <option value="">-----Select book location-----</option>
              @foreach($allLocation as $location)
                <option value="{{$location->id}}">
                  {{$location->location}}
                </option>
              @endforeach
            </select>
          </td>
        </tr>
        <tr>
          <td class="active">Date</td>
          <td>
            <input class="form-control" type="text" name="detail_date_buy" id="detail_date_buy" value="">
          </td>
        </tr>

        <tr>
          <td class="active">Translator</td>
          <td>
            <select class="form-control" name="detail_translator" id="detail_translator">
              <option value="">-----Select Translator -----</option>
              @foreach($allTranslator as $tran)
                <option value="{{$tran->transID}}">
                  {{$tran->translator_name}}
                </option>
              @endforeach
            </select>
          </td>
        </tr>

        <tr>
            <td class="active">Price</td>
            <td>
              <input class="form-control" type="text" name="detail_price" id="detail_price" value="">
            </td>
        </tr>

        <tr>
            <td class="active">Quan.</td>
            <td>
              <input class="form-control" type="text" name="detail_quan" id="detail_quan" value="">
            </td>
        </tr>

        <tr>
            <td class="active">Photo</td>
            <td>
              <input class="form-control" type="text" name="detail_photo" id="detail_photo" value="">
            </td>
        </tr>

        <tr>
            <td class="active">Read Flg</td>
            <td>
                <input type="checkbox" name="detail_read_flg" id="detail_read_flg" value="1">
            </td>
        </tr>

        <tr>
          <td colspan="2" align="center">
            <input type="hidden" name="mangaDetailID" id="mangaDetailID" value="">
            <input type="hidden" value="editDetail" name="mode">
            <button class="btn btn-primary btn-sm" type="submit" id="save_manga_detail"  >Save</button>
            <button class="btn btn-primary btn-sm" type="button" id="close_manga_detail" data-dismiss="modal" >Close</button>
          </td>
        </tr>
      </table>
    </form>
</div>