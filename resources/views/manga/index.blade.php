@extends('layouts.master')

@section('content')
<div class="row">
    <div class="col-lg-12">
        <h3 class="page-header">Manga</h3>
    </div>
    <!-- /.col-lg-12 -->
</div>

<div class="row">
      <div class="panel panel-default">
        <div class="panel-heading clearfix">
          <form class="navbar-form navbar-left" role="search">
            {{ csrf_field() }}
            <div class="form-group">
              <input type="text" name="search_name" id="search_name" class="form-control" placeholder="Search" value="{{$search_name}}">
            </div>
            <div class="form-group">
              <select class="form-control" name="author_id" id="author_id">
                <option value="">----------------</option>
                @foreach($all_author as $author)
                  <option value="{{$author->id}}" {{ $author_id == $author->id ? 'selected="selected"' : '' }} >
                    {{$author->name}}
                  </option>
                @endforeach
              </select>
            </div>

            <button type="submit" class="btn btn-primary btn-sm">Search</button>
            <button type="button" class="btn btn-primary btn-sm" id="reset_book_search">Reset</button>
          </form>

          <div class="navbar-form navbar-right">
            <a  href="/manga/insert" class="btn btn-info btn-sm" target="_blank">Add a new</a>
          </div>
        </div>
        <!-- panel-heading clearfix -->
      </div>
      <!-- panel panel-default -->

      <!-- Table -->
      <table class="table table-striped table-bordered table-condensed">
        <thead>
          <tr>
            <th>#</th>
            <th>Name</th>
            <th>Eng Name</th>
            <th>Native Name</th>
            <th>Latest Vol.</th>
            <th>Story</th>
            <th>Illustrator</th>
            <th>&nbsp;</th>
          </tr>
        </thead>
        <tbody>
            @if (isset($manga_arr))
                @foreach($manga_arr as $manga)
                    <tr>
                        <td>
                            {{$loop->iteration + $paginator->firstItem()-1}}
                        </td>
                        <td>
                          {{$manga->mangaName}}
                          @if($manga->mangaStatus == 'S')
                            <span class="label label-danger">Sold</span>
                          @endif
                         </td>
                        <td>{{$manga->mangaNameEng}}</td>
                        <td>{{$manga->mangaNativeName}}</td>
                        <td class="text-center">{{$manga->latest_vol}}</td>
                        <td>
                          @foreach($manga->author as $item)
                            @if($item->role_id == 1)
                              <a href="/manga?author_id={{$item->author_id}}">{{$item->authors->name}}</a>
                              <br>
                            @endif
                          @endforeach
                        </td>
                        <td>
                          @foreach($manga->author as $item)
                            @if($item->role_id == 2)
                              <a href="/manga?author_id={{$item->author_id}}">{{$item->authors->name}}</a>
                              <br>
                            @endif
                          @endforeach
                        </td>
                        <td>
                          <a href="/manga/view/{{$manga->mangaID}}" target="_blank" class="btn btn-info btn-xs" >View</a> 

                          @if($manga->externalWebID != '')
                            <a href="https://www.mangaupdates.com/series.html?id={{$manga->externalWebID}}" class="fa fa-external-link fa-fw" target="_blank"></a>
                          @endif
                        </td>
                    </tr>
                @endforeach
            @endif
        </tbody>
      </table>
      <nav style="text-align:center">{{$paginator->appends(request()->except('_token'))->links()}}</nav>
</div> <!-- row -- >
@endsection

@section('pagescript')
  <script src="{{asset('jquery-ui/jquery-ui.min.js')}}"></script>
  <script src="{{asset('js/jquery.validate.min.js')}}"></script>
  <script src="{{asset('js/manga.js')}}"></script>
@endsection

@push('styles')
  <link href="{{asset('jquery-ui/jquery-ui.min.css')}}" rel="stylesheet" type="text/css">
  <link href="{{asset('css/manga.css')}}" rel="stylesheet" type="text/css">
@endpush
