@extends('layouts.master')
@section('content')
<div class="row">
        <div class="col-lg-12">
            <h3 class="page-header">Insert Manga</h3>
        </div>
        <!-- /.col-lg-12 -->
    </div>

    <div class="row">
        <div class="panel panel-default">
        <div class="panel-heading clearfix">
            @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif

        <form action="" method="POST" >
            {{ csrf_field() }}
            <div class="form-group">
                <label for="mangaCode">Code</label>
                <input type="text" class="form-control" id="mangaCode" name="mangaCode" value="01">
            </div>

            <div class="form-group">
                <label for="mangaName">Name</label>
                <input type="text" class="form-control" id="mangaName" name="mangaName" value="{{ old('mangaName') }}">
            </div>

            <div class="form-group">
                <label for="mangaNameEng">Name (Eng)</label>
                <input type="text" class="form-control" id="mangaNameEng" name="mangaNameEng" value="{{ old('mangaNameEng') }}">
            </div>

            <div class="form-group">
                <label for="mangaNativeName">Name (Native)</label>
                <input type="text" class="form-control" id="mangaNativeName" name="mangaNativeName" value="{{ old('mangaNativeName') }}">
            </div>

            <div class="form-group">
                <label class="control-label" for="author_id">
                    Author <a class="label label-primary" target="_blank" href="/author/insert">Add New</a>
                </label>
                <input
                class="form-control bs-autocomplete"
                id="ac-demo"
                name="author_name"
                value="{{ old('author_name') }}"
                placeholder=""
                type="text"
                data-source="/ajax/callAuthor"
                data-hidden_field_id="author_id"
                data-item_id="id"
                data-item_label="authorName"
                autocomplete="off"
                >
                <input class="form-control" id="author_id" name="author_id" value="{{ old('author_id') }}" type="hidden" >

                <span id="helpBlock" class="help-block">
                - doraemon<br/>
                - adfdfasf<br/>
                - adfasdfasdf
                </span>
            </div>

            <div class="form-group">
                <label class="control-label" for="artist_id">Artist</label>
                <input
                class="form-control bs-autocomplete"
                id="ac-demo2"
                name="artist_name"
                value="{{ old('artist_name') }}"
                placeholder=""
                type="text"
                data-source="/ajax/callAuthor"
                data-hidden_field_id="artist_id"
                data-item_id="id"
                data-item_label="authorName"
                autocomplete="off"
                >
                <input class="form-control" id="artist_id" name="artist_id" value="{{ old('artist_id') }}" type="hidden" >

                <span id="helpBlock" class="help-block">
                - doraemon<br/>
                - adfdfasf<br/>
                - adfasdfasdf
                </span>
            </div>

            <div class="form-group">
                <label class="control-label" for="char_id">Charactor design</label>
                <input
                class="form-control bs-autocomplete"
                id="ac-demo3"
                name="char_name"
                value="{{ old('char_name') }}"
                placeholder=""
                type="text"
                data-source="/ajax/callAuthor"
                data-hidden_field_id="char_id"
                data-item_id="id"
                data-item_label="authorName"
                autocomplete="off"
                >
                <input class="form-control" id="char_id" name="char_id" value="{{ old('char_id') }}" type="hidden" >

                <span id="helpBlock" class="help-block">
                - doraemon<br/>
                - adfdfasf<br/>
                - adfasdfasdf
                </span>
            </div>

            <div class="form-group">
                <label class="control-label" for="publishID">Publish</label>
                <select class="form-control" name="publishID" id="publishID">
                    <option value="">----------------</option>
                    @foreach($allPublish as $publish)
                        <option
                        value="{{$publish->id}}"
                        {{(old('publishID') == $publish->id ? "selected":"")}}
                        >
                        {{$publish->name}}
                        </option>
                    @endforeach
                </select>
            </div>

            <div class="form-group">
                <label class="control-label" for="oriPublishID">Original Publish</label>
                <select class="form-control" name="oriPublishID" id="oriPublishID">
                    <option value="">----------------</option>
                    @foreach($allOriPublish as $item)
                        <option
                        value="{{$item->id}}"
                        {{(old('oriPublishID') == $item->id ? "selected":"")}}
                        >
                        {{$item->name}}
                        </option>
                    @endforeach
                </select>
            </div>

            <div class="form-group">
                <label class="control-label" for="mangaStatus">Status</label>
                <select class="form-control" name="mangaStatus" id="mangaStatus">
                    <option value="O" >Ongoing</option>
                    <option value="C" >Complete</option>
                </select>
            </div>

            <div class="form-group">
                <label for="externalWebID">External web</label>
                <input type="text" class="form-control" id="externalWebID" name="externalWebID" value="{{ old('externalWebID') }}">
            </div>

            <div class="form-group">
                <label for="mangaVol">Volumn</label>
                <input type="text" class="form-control" id="mangaVol" name="mangaVol" value="1">
            </div>

            <div class="form-group">
                <label for="mangaPrice">Price</label>
                <input type="text" class="form-control" id="mangaPrice" name="mangaPrice" value="{{ old('mangaPrice') }}">
            </div>

            <div class="form-group">
                <label for="mangaDateBuy">Date</label>
                <input
                type="text"
                class="form-control oyTest"
                id="date_buy"
                name="mangaDateBuy"
                @if(old('mangaDateBuy') != '')
                    value="{{ old('mangaDateBuy') }}"
                @else
                    value="{{$currentDate}}"
                @endif
                >
            </div>

            <input type="hidden" name="mode" value="insert">
            <button type="submit" class="btn btn-primary">Submit</button>
            <button class="btn btn-default" type="reset">Reset</button>
        </form>
      </div></div>
    </div>
@endsection

@section('pagescript')
<script src="{{asset('jquery-ui/jquery-ui.min.js')}}"></script>
<script src="{{asset('js/jquery.validate.min.js')}}"></script>
<script src="{{asset('js/manga.js')}}"></script>
<script src="{{asset('js/call-autocomplete.js')}}"></script>
@endsection

@push('styles')
  <link href="{{asset('jquery-ui/jquery-ui.min.css')}}" rel="stylesheet" type="text/css">
  <link href="{{asset('css/manga.css')}}" rel="stylesheet" type="text/css">
@endpush