@extends('layouts.master')
@section('content')
<div class="row">
  <div class="col-lg-12">
      <h3 class="page-header">View Manga</h3>
  </div>
  <!-- /.col-lg-12 -->
</div>

<div class="row">

@if (Session::has('flash_message'))
  @if(Session::get('flash_message') == 'updated')
    <div class="alert alert-success">
      <a class="close" data-dismiss="alert">×</a>
      <strong>Well done!</strong> update manga info success.
    </div>
  @endif

  @if(Session::get('flash_message') == 'new_vol_inserted')
    <div class="alert alert-success">
      <a class="close" data-dismiss="alert">×</a>
      <strong>Well done!</strong> insert manga new volumn success.
    </div>
  @endif

  @if(Session::get('flash_message') == 'all_loc_updated')
    <div class="alert alert-success">
      <a class="close" data-dismiss="alert">×</a>
      <strong>Well done!</strong> all location update success.
    </div>
  @endif

  @if(Session::get('flash_message') == 'detail_updated')
    <div class="alert alert-success">
      <a class="close" data-dismiss="alert">×</a>
      <strong>Well done!</strong> update manga detail success.
    </div>
  @endif

@endif

<div class="col-lg-6" id="manga_edit_div">
  <div class="panel panel-primary">
    <div class="panel-heading">
        <small>Information</small> #{{$mangaInfo->mangaID}}
        @if($mangaInfo->remark != '')
            <button
                    type="button"
                    class="btn btn-primary btn-xs pull-right"
                    data-toggle="modal"
                    data-target="#remarkModal"
                    data-manga-id="{{$mangaInfo->mangaID}}">
                <i class="fa fa-info-circle fa-lg " ></i>
            </button>
        @endif
    </div>
      <div class="panel-body">
        <div class="row manga-info">
          <!-- Main Info -->
          <div class="col-xs-12">
            <h4 class="manga-name">{{$mangaInfo->mangaName}}</h4>
            <h5 class="manga-alt-name text-muted">
              <small>{{$mangaInfo->mangaNameEng}}</small>
            </h5>
            <h5 class="manga-alt-name text-muted">
              <small>[ {{$mangaInfo->mangaNativeName}} ]</small>
            </h5>
          </div>

          <!-- Status -->
          <div class="col-xs-12 col-md-6 col-lg-4">
            <div class="info-block">
              <label class="lbl">Status</label>
              <div class="data">
                @if($mangaInfo->mangaStatus == 'S')
                  <span class="label label-danger">{{$status}}</span>
                @elseif($mangaInfo->mangaStatus == 'C')
                  <span class="label label-success">{{$status}}</span>
                @else
                  {{$status}}
                @endif
              </div>
            </div>
          </div>

          <!-- Auther -->
          <div class="col-xs-12 col-md-6 col-lg-4">
            <div class="info-block">
              <label class="lbl">Author(s)</label>
              @foreach($authors as $item)
                <div class="data">{{$item->authors->name}}</div>
              @endforeach
            </div>
          </div>

          <!-- Required! -->
          <div class="clearfix visible-md"></div>

          <!-- Artist -->
          <div class="col-xs-12 col-md-6 col-lg-4">
            <div class="info-block">
              <label class="lbl">Artist(s) / Character design</label>
              @foreach($artists as $item)
                <div class="data">{{$item->authors->name}}</div>
              @endforeach
              
              @if(count($chardesigns) > 0)
                @foreach($chardesigns as $item)
                  <div class="data">{{$item->authors->name}}</div>
                @endforeach
              @endif
            </div>
          </div>

          <!-- Publisher (TH) -->
          <div class="col-xs-12 col-md-6 col-lg-4">
            <div class="info-block">
              <label class="lbl">Publisher (TH)</label>
              <div class="data">{{$publish->name}}</div>
            </div>
          </div>

          <!-- Required! -->
          <div class="clearfix visible-md"></div>

          <!-- Publisher (Alt) -->
          <div class="col-xs-12 col-md-6 col-lg-4">
            <div class="info-block">
              <label class="lbl">Publisher (Original)</label>
              <div class="data">{{@$oriPublish->name}}</div>
            </div>
          </div>

          <!-- Reference (optional) -->
          <div class="col-xs-12 col-md-6 col-lg-4">
            <div class="info-block">
              <label class="lbl">Reference</label>
              @if($mangaInfo->externalWebID != '')

              <a target="_blank" href="https://www.mangaupdates.com/series.html?id={{$mangaInfo->externalWebID}}">
                <span
                  class="fa fa-external-link fa-fw"
                  aria-hidden="true"
                ></span>
              </a>
              @else
                -
              @endif
            </div>
          </div>

          <!-- Required! -->
          <div class="clearfix visible-md visible-lg"></div>

          <!-- Image (if any) -->
          @if($mangaDetail[0]->mangaPhoto != '')
            <div class="col-xs-12 col-md-6 col-lg-3">
              <div class="info-block">
                <label class="lbl">Cover</label>
                <img class="img-responsive"
                src="/images/manga/{{$mangaInfo->mangaID}}/{{$mangaDetail[0]->mangaPhoto}}" />
              </div>
            </div>
          @endif

          <!-- Category (if any) -->
          @if(count($bookCategory) > 0)
          <div class="col-xs-12 col-md-6 col-lg-9">
            <div class="info-block">
              <label class="lbl">Category</label>
              @foreach($bookCategory as $val)
                <span class="badge">{{$val->category->catName}}</span>
              @endforeach
            </div>
          </div>
          @endif

          <div class="col-xs-12">
            <hr>
            <button
                    class="btn btn-primary btn-sm btn-block"
                    type="button"
                    data-toggle="modal"
                    data-target="#editModal"
                    data-manga-id="{{$mangaInfo->mangaID}}">
                Edit
            </button>
          </div>
        </div>
      </div>
    </div>
</div>


@if($mangaInfo->mangaStatus != 'C')
<div class="col-lg-6">
  <form id="manga_new_vol_frm" action="" method="POST">
    {{ csrf_field() }}
    <div class="panel panel-info">
      <div class="panel-heading">New volumn</div>
        <table class="table table-bordered" >
          <tr>
            <td>Vol.</td>
            <td><input class="form-control" type="text" name="vol" id="vol" value="{{$nextVol}}"></td>
          </tr>
          <tr>
            <td>Price</td>
            <td><input class="form-control" type="text" name="price" id="price" value="{{$latestPrice}}"></td>
          </tr>
          <tr>
            <td>Date Buy</td>
            <td>
              <input class="form-control" type="text" name="date_buy" id="date_buy" value="{{$currentDate}}">
            </td>
          </tr>
          <tr>
            <td colspan="2" align="center">
              <input type="hidden" name="mode" value="newVol">
              <button class="btn btn-info btn-sm" id="save_new_vol" type="submit" data-manga-id="">Save</button>
            </td>
          </tr>
        </table>
    </div>  <!-- panel panel-info -->
  </form>
</div> <!-- col-lg-6 -->
@endif

<div class="col-lg-6">
  <div class="panel panel-info">
    <div class="panel-heading">Update all location</div>
    <div class="panel-body">
      <form id="all_location_frm" action="" method="POST">
        {{ csrf_field() }}
        <div class="form-group">
          <select class="form-control" name="all_location">
            <option value="">-----Select book location-----</option>
            @foreach($allLocation as $location)
              <option value="{{$location->id}}">
                {{$location->location}}
              </option>
            @endforeach
          </select>
        </div>
        <input type="hidden" name="mode" value="allLoc">
        <button type="submit" class="btn btn-primary" data-manga-id="" id="update_all_location">Update all seies</button>
      </form>
    </div>
  </div>
</div>

</div> <!-- row -->



<div class="row">
<div class="panel panel-info">

  <div class="panel-heading">
    <div class="nav">
      <div class="pull-left">Volumn detail</div>
      <div class="pull-right">
        <button
                type="button"
                class="btn btn-primary btn-xs"
                data-toggle="modal"
                data-target="#allImageModal"
                data-whatever="{{$mangaInfo->mangaID}}">
                <i class="glyphicon glyphicon-picture" ></i> All Photo
        </button>
      </div>
    </div>
  </div>

  <div class="panel-body">

    @foreach($mangaDetail as $detail)
    <div class="col-md-3">

     <div class="thumbnail clearfix" id="thumb_{{$detail->mangaVol}}">

        <div class="form-group">
          <span class="manga-volume">Vol. {{$detail->mangaVol}} </span>
          @if($detail->readFlg == 1)
            <span class="label label-success">Read</span>
          @else
            <span class="label label-default">Unread</span>
          @endif

          @if($mangaDetail->count() > 1)
          <button type="button" class="close pull-right" data-toggle="modal" data-target="#deleteModal" data-whatever="">&times; </button>
          @endif
        </div>

        <div class="form-group">
          <label>Location :</label>
          <span>
          {{@$detail->location->location}}
          @if($detail->mangaLocation == 9)
            <i class="fa fa-cloud fa-lg" aria-hidden="true"></i>
          @endif
          </span>
        </div>

        <div class="form-group">
          <label>Date :</label>
          <span>
            {{$detail->mangaDateBuy}}
          </span>
        </div>
        <div class="form-group">
          <label>Translator :</label>
          <span>{{@$detail->translator->translator_name}}</span>
        </div>

        @if($detail->mangaPhoto != '')
        <button type="button" class="btn btn-default btn-xs pull-left" data-toggle="modal" data-target="#imageModal"
                data-manga-id="/images/manga/{{$detail->mangaID}}/{{$detail->mangaPhoto}}"><i class="glyphicon glyphicon-picture" ></i> Photo</button>
        @endif

        <button type="button" class="btn btn-info btn-xs pull-right" data-toggle="modal" data-target="#editDetailModal" data-manga-id="{{$detail->mangaDetailID}}">
          <i class="glyphicon glyphicon-pencil"></i> Edit
        </button>

      </div>
      <!-- thumbnail -->

    </div>
    <!-- col-md-4 -->
    @endforeach

  </div>
  <!-- panel-body -->
</div>
<!-- panel panel-info -->
</div>
<!-- row -->

<!-- edit form -->
<div class="modal fade" id="editModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title" id="myModalLabel">Edit manga info</h4>
        </div>
        <div class="modal-body">
            @include('manga.edit')
        </div>
        <div class="modal-footer">
          &nbsp;
        </div>
      </div>
  </div>
</div>
<!-- edit form -->

<!-- edit form -->
<div class="modal fade" id="editDetailModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title" id="myModalLabel">Volumn <span id="detail_volumn"></span></h4>
        </div>
        <div class="modal-body">
            @include('manga.editdetail')
        </div>
        <div class="modal-footer">
          &nbsp;
        </div>
      </div>
  </div>
</div>
<!-- edit form -->

<div class="modal fade" id="deleteModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
<div class="modal-dialog modal-sm">
  <div class="modal-content">
    <div class="modal-header">
      <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
      <h4 class="modal-title" id="myModalLabel">Delete Volumn</h4>
    </div>
    <div class="modal-body">
      Do want to delete this item ?
      <input type="hidden" value="" id="hidden_manga_detail_id" name="hidden_manga_detail_id" />
    </div>
    <div class="modal-footer">
      <button type="button" class="btn btn-default btn-xs" data-dismiss="modal">
        <i class="glyphicon glyphicon-remove"></i> Cancel
      </button>
      <button type="button" class="btn btn-danger btn-xs" id="deleteVol" data-dismiss="">
        <i class="glyphicon glyphicon glyphicon-remove" ></i> Delete
      </button>
    </div>
  </div>
</div>
</div>

<div class="modal fade" id="imageModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-sm">
    <div class="modal-content">
      <div class="modal-header" >
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
      </div>

      <div class="modal-body">
        <center><img class="img-responsive" src="" id="show_image"></center>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="allImageModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header" >
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
      </div>
      <div class="modal-body">
        <div class="row">
            @foreach($mangaDetail as $item)
            <div class="col-md-3">
              <div class="thumbnail clearfix">
                  <span class="manga-volume">Vol. {{$item->mangaVol}} </span>
                  @if($item->mangaPhoto != '')
                  <img class="img-responsive" src="/images/manga/{{$item->mangaID}}/{{$item->mangaPhoto}}">
                  @endif
              </div>
              <!-- thumbnail -->
            </div>
            <!-- col-md-3 -->
            @endforeach
        </div>
      </div>

      <div class="modal-footer" >
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
      </div>
    </div>
  </div>
</div>

<div class="modal fade"
     id="remarkModal"
     tabindex="-1" role="dialog"
     aria-labelledby="myModalLabel"
     aria-hidden="true">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-body">
                <div id="showRemark"></div>
            </div>
        </div>
    </div>
</div>

@endsection

@section('pagescript')
<script src="{{asset('jquery-ui/jquery-ui.min.js')}}"></script>
<script src="{{asset('js/jquery.validate.min.js')}}"></script>
<script src="{{asset('js/manga.js')}}"></script>
<script src="{{asset('js/call-autocomplete.js')}}"></script>
@endsection


@push('styles')
  <link href="{{asset('jquery-ui/jquery-ui.min.css')}}" rel="stylesheet" type="text/css">
  <link href="{{asset('css/manga.css')}}" rel="stylesheet" type="text/css">
@endpush