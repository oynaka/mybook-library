@extends('layouts.master')
@section('content')
<div class="row">
  <div class="col-lg-12">
    <h3 class="page-header">
      New books within 3 months
      <a class="btn btn-default btn-xs" href="/newbook/raw" target="_blank" role="button">Raw Data</a>
    </h3>
  </div>
  <!-- /.col-lg-12 -->
</div>

<div class="row">
  <!-- Nav tabs -->
  <ul class="nav nav-pills">
    <li class="active">
        <a href="#manga" data-toggle="tab"><strong>Manga</strong> <span class="badge">{{$totalManga}}</span></a>
    </li>
    <li>
        <a href="#novel" data-toggle="tab"><strong>Novel</strong> <span class="badge">{{$totalNovel}}</span></a>
    </li>
  </ul>

  <!-- Tab panes -->
  <div class="tab-content">
      
    <div class="tab-pane fade in active" id="manga">
      <br/>
      <div class="panel panel-info">
      <div class="panel-heading">&nbsp;</div>
      <table class="table table-condensed table-bordered table-hover" >
        <thead>
          <tr>
            <th>#</th>
            <th>Name</th>
            <th>Name Eng</th>
            <th>Vol.</th>
            <th>Date Buy</th>
            <th>&nbsp;</th>
          </tr>
        </thead>
        <tbody>
          @if (isset($mangaList) && count($mangaList)>0)
              @foreach($mangaList as $manga)
                  <tr>
                      <td class="text-center">{{$loop->iteration}}</td>
                      <td>{{$manga->mangaName}}</td>
                      <td>{{$manga->mangaNameEng}}</td>
                      <td class="text-center">{{$manga->mangaVol}}</td>
                      <td>{{$manga->mangaDateBuy}}</td>
                      <td>
                        <a href="/manga/view/{{$manga->mangaID}}" class="btn btn-info btn-xs" target="_blank" >view</a>
                      </td>
                  </tr>
              @endforeach
          @else
              <tr><td colspan="5">&nbsp;</td></tr>
          @endif
        </tbody>
      </table>
      </div>
    </div>
    <!-- /.tab-pane fade in  -->

    <div class="tab-pane fade in " id="novel">
      <br/>
      <div class="panel panel-success">
      <div class="panel-heading">&nbsp;</div>
      <table class="table table-condensed table-bordered table-hover" >
        <thead>
          <tr>
            <th>#</th>
            <th>Name</th>
            <th>Native Name</th>
            <th>Vol.</th>
            <th>Date Buy</th>
            <th>&nbsp;</th>
          </tr>
        </thead>
          @if (isset($novelList) && count($novelList)>0)
            @foreach($novelList as $novel)
              <tr>
                <td class="text-center">{{$loop->iteration}}</td>
                <td>{{$novel->novelName}}</td>
                <td>{{$novel->novelNativeName}}</td>
                <td class="text-center">{{$novel->novelVol}}</td>
                <td>{{$novel->novelDateBuy}}</td>
                <td>
                    <a href="/novel/view/{{$novel->novelID}}" class="btn btn-info btn-xs" target="_blank" >view</a>
                    <a href="/unread/edit/{{$novel->novelDetailID}}?t=2" class="btn btn-success btn-xs" >read</a>
                </td>
              </tr>
            @endforeach
          @else
              <tr><td colspan="5">&nbsp;</td></tr>
          @endif
        </tbody>
      </table>
      </div>
    </div>
    <!-- /.tab-pane fade in  -->
      
  </div>
</div>
<!-- /.row -->
@endsection