@extends('layouts.master')
@section('content')
<div class="row">
  <div class="col-lg-12">
    <h3 class="page-header">
      New books within 3 months (raw display)
    </h3>
  </div>
  <!-- /.col-lg-12 -->
</div>

<div class="row">
  <!-- Nav tabs -->
  <ul class="nav nav-pills">
    <li class="active">
        <a href="#manga" data-toggle="tab"><strong>Manga</strong> <span class="badge">{{$totalManga}}</span></a>
    </li>
    <li>
        <a href="#novel" data-toggle="tab"><strong>Novel</strong> <span class="badge">{{$totalNovel}}</span></a>
    </li>
  </ul>

  <!-- Tab panes -->
  <div class="tab-content">
      
    <div class="tab-pane fade in active" id="manga">
      <br/>
      <div class="panel panel-info">
      <div class="panel-heading">&nbsp;</div>
      <div class="panel-body">
        @if (isset($mangaList) && count($mangaList)>0)
          @foreach($mangaList as $key => $list)
            <span class="label label-primary">{{$key}}</span>
            <div class="well well-sm">
            @foreach($list as $item)
              {{$item->mangaName}} {{$item->mangaVol}}<br/>
            @endforeach
            </div>
          @endforeach
        @endif
      </div>
      </div>
    </div>
    <!-- /.tab-pane fade in  -->

    <div class="tab-pane fade in " id="novel">
      <br/>
      <div class="panel panel-success">
      <div class="panel-heading">&nbsp;</div>
      <div class="panel-body">
        @if (isset($novelList) && count($novelList)>0)
          @foreach($novelList as $key => $list)
          <span class="label label-success">{{$key}}</span>
            <div class="well well-sm">
            @foreach($list as $item)
              {{$item->novelName}} {{$item->novelVol}}<br/>
            @endforeach
            </div>
          @endforeach
        @endif
      </div>
      </div>
    </div>
    <!-- /.tab-pane fade in  -->
      
  </div>
</div>
<!-- /.row -->
@endsection