@if ($errors->any())
<div class="alert alert-danger">
    <ul>
        @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
        @endforeach
    </ul>
</div>
@endif
<div class="panel panel-primary">
    <div class="panel-heading">Information</div>
    <form id="novel_info_frm" action="" method="POST">
      {{ csrf_field() }}
      <table class="table table-bordered" >
        <tr>
          <td class="active" width="30%">ID</td>
          <td>{{$novelInfo->novelID}}</td>
        </tr>
        <tr>
          <td class="active">Name</td>
          <td>
            <input class="form-control" type="text" name="novelName" id="novelName" value="{{$novelInfo->novelName}}">
          </td>
        </tr>
        <tr>
          <td class="active">Name Eng</td>
          <td><input class="form-control" type="text" name="novelNameEng" id="novelNameEng" value="{{$novelInfo->novelNameEng}}"></td>
        </tr>
        <tr>
          <td class="active">Native Name</td>
          <td><input class="form-control" type="text" name="novelNativeName" id="novelNativeName" value="{{$novelInfo->novelNativeName}}"></td>
        </tr>

        <tr>
          <td class="active">Author</td>
          <td>
            <input
              class="form-control bs-autocomplete"
              id="ac-demo"
              value="{{$authors[0]->authors->name}}"
              placeholder=""
              type="text"
              data-source="/ajax/callAuthor"
              data-hidden_field_id="author_id"
              data-item_id="id"
              data-item_label="authorName"
              autocomplete="off"
            >
            <input class="form-control" id="author_id" name="author_id" value="{{$authors[0]->author_id}}" type="hidden" >
          </td>
        </tr>

        <tr>
          <td class="active">Publish</td>
          <td>
            <select class="form-control" name="publishID" id="publishID">
              <option value="">----------------</option>
              @foreach($allPublish as $publish)
                <option value="{{$publish->id}}"
                  {{ $novelInfo->publishID == $publish->id ? 'selected="selected"' : '' }}
                >
                  {{$publish->name}}
                </option>
              @endforeach
            </select>
          </td>
        </tr>

        <tr>
          <td class="active">Status</td>
          <td>
            <select class="form-control" name="novelStatus" id="novelStatus">
              @foreach($allStatus as $status)
                <option value="{{$status['id']}}"
                  {{ $novelInfo->novelStatus == $status['id'] ? 'selected="selected"' : '' }}
                >
                  {{$status['name']}}
                </option>
              @endforeach
            </select>
          </td>
        </tr>

        <tr>
          <td colspan="2" align="center">
            <input type="hidden" value="edit" name="mode">
            <button class="btn btn-primary btn-sm" type="submit" id="save_novelInfo"  >Save</button>
            <button class="btn btn-primary btn-sm" type="button" id="close_novelInfo" data-dismiss="modal" >Close</button>
          </td>
        </tr>
      </table>
    </form>
</div>