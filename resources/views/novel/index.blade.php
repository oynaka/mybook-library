@extends('layouts.master')

@section('content')
<div class="row">
    <div class="col-lg-12">
        <h3 class="page-header">novel</h3>
    </div>
    <!-- /.col-lg-12 -->
</div>

<div class="row">
      <div class="panel panel-default">
        <div class="panel-heading clearfix">
          <form class="navbar-form navbar-left" role="search">
            {{ csrf_field() }}
            <div class="form-group">
              <input type="text" name="search_name" id="search_name" class="form-control" placeholder="Search" value="{{$search_name}}">
            </div>
            <div class="form-group">
              <select class="form-control" name="author_id" id="author_id">
                <option value="">----------------</option>
                @foreach($all_author as $author)
                  <option value="{{$author->id}}" {{ $author_id == $author->id ? 'selected="selected"' : '' }} >
                    {{$author->name}}
                  </option>
                @endforeach
              </select>
            </div>

            <button type="submit" class="btn btn-primary btn-sm">Search</button>
            <button type="button" class="btn btn-primary btn-sm" id="reset_book_search">Reset</button>
          </form>

          <div class="navbar-form navbar-right">
            <a  href="/novel/insert" class="btn btn-info btn-sm" target="_blank">Add a new</a>
          </div>
        </div>
        <!-- panel-heading clearfix -->
      </div>
      <!-- panel panel-default -->

      <!-- Table -->
      <table class="table table-striped table-bordered table-condensed">
        <thead>
          <tr>
            <th>#</th>
            <th>Name</th>
            <th>Eng Name</th>
            <th>Native Name</th>
            <th>Latest Vol.</th>
            <th>Story</th>
            <th>&nbsp;</th>
          </tr>
        </thead>
        <tbody>
            @if (isset($novel_arr))
                @foreach($novel_arr as $novel)
                    <tr>
                        <td>
                            {{$loop->iteration + $paginator->firstItem()-1}}
                        </td>
                        <td>{{$novel->novelName}}</td>
                        <td>{{$novel->novelNameEng}}</td>
                        <td>{{$novel->novelNativeName}}</td>
                        <td class="text-center">{{$novel->latest_vol}}</td>
                        <td>
                          @foreach($novel->author as $item)
                            <a href="/novel?author_id={{$item->author_id}}">{{$item->authors->name}}</a>
                            <br>
                          @endforeach
                        </td>
                        <td>
                          <a href="/novel/view/{{$novel->novelID}}" target="_blank" class="btn btn-info btn-xs" >View</a>
                        </td>
                    </tr>
                @endforeach
            @endif
        </tbody>
      </table>
      <nav style="text-align:center">{{$paginator->appends(request()->except('_token'))->links()}}</nav>
</div> <!-- row -- >
@endsection

@section('pagescript')
  <script src="{{asset('jquery-ui/jquery-ui.min.js')}}"></script>
  <script src="{{asset('js/jquery.validate.min.js')}}"></script>
  <script src="{{asset('js/novel.js')}}"></script>
@endsection

@push('styles')
  <link href="{{asset('jquery-ui/jquery-ui.min.css')}}" rel="stylesheet" type="text/css">
  <link href="{{asset('css/novel.css')}}" rel="stylesheet" type="text/css">
@endpush
