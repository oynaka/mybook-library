@extends('layouts.master')
@section('content')
<div class="row">
        <div class="col-lg-12">
            <h3 class="page-header">Insert Novel</h3>
        </div>
        <!-- /.col-lg-12 -->
    </div>

    <div class="row">
        <div class="panel panel-default">
        <div class="panel-heading clearfix">
            @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif

        <form action="" method="POST" >
            {{ csrf_field() }}
            <div class="form-group">
                <label for="novelCode">Code</label>
                <input type="text" class="form-control" id="novelCode" name="novelCode" value="01">
            </div>

            <div class="form-group">
                <label for="novelName">Name</label>
                <input type="text" class="form-control" id="novelName" name="novelName" value="{{ old('novelName') }}">
            </div>

            <div class="form-group">
                <label for="novelNameEng">Name (Eng)</label>
                <input type="text" class="form-control" id="novelNameEng" name="novelNameEng" value="{{ old('novelNameEng') }}">
            </div>

            <div class="form-group">
                <label for="novelNativeName">Name (Native)</label>
                <input type="text" class="form-control" id="novelNativeName" name="novelNativeName" value="{{ old('novelNativeName') }}">
            </div>

            <div class="form-group">
                <label class="control-label" for="author_id">
                    Author <a class="label label-primary" target="_blank" href="/author/insert">Add New</a>
                </label>
                <input
                class="form-control bs-autocomplete"
                id="ac-demo"
                name="author_name"
                value="{{ old('author_name') }}"
                placeholder=""
                type="text"
                data-source="/ajax/callAuthor"
                data-hidden_field_id="author_id"
                data-item_id="id"
                data-item_label="authorName"
                autocomplete="off"
                >
                <input class="form-control" id="author_id" name="author_id" value="{{ old('author_id') }}" type="hidden" >

                <span id="helpBlock" class="help-block">
                - doraemon<br/>
                - adfdfasf<br/>
                - adfasdfasdf
                </span>
            </div>

            <div class="form-group">
                <label class="control-label" for="publish_id">Publish</label>
                <select class="form-control" name="publishID" id="publishID">
                    <option value="">----------------</option>
                    @foreach($allPublish as $publish)
                        <option
                        value="{{$publish->id}}"
                        {{(old('publishID') == $publish->id ? "selected":"")}}
                        >
                        {{$publish->name}}
                        </option>
                    @endforeach
                </select>
            </div>

            <div class="form-group">
                <label class="control-label" for="novelStatus">Status</label>
                <select class="form-control" name="novelStatus" id="novelStatus">
                    <option value="O" >Ongoing</option>
                    <option value="C" >Complete</option>
                </select>
            </div>

            <div class="form-group">
                <label for="novelVol">Volumn</label>
                <input type="text" class="form-control" id="novelVol" name="novelVol" value="1">
            </div>

            <div class="form-group">
                <label for="novelPrice">Price</label>
                <input type="text" class="form-control" id="novelPrice" name="novelPrice" value="{{ old('novelPrice') }}">
            </div>

            <div class="form-group">
                <label for="novelDateBuy">Date</label>
                <input
                type="text"
                class="form-control oyTest"
                id="date_buy"
                name="novelDateBuy"
                @if(old('novelDateBuy') != '')
                    value="{{ old('novelDateBuy') }}"
                @else
                    value="{{$currentDate}}"
                @endif
                >
            </div>

            <input type="hidden" name="mode" value="insert">
            <button type="submit" class="btn btn-primary">Submit</button>
            <button class="btn btn-default" type="reset">Reset</button>
        </form>
      </div></div>
    </div>
@endsection

@section('pagescript')
<script src="{{asset('jquery-ui/jquery-ui.min.js')}}"></script>
<script src="{{asset('js/jquery.validate.min.js')}}"></script>
<script src="{{asset('js/novel.js')}}"></script>
<script src="{{asset('js/call-autocomplete.js')}}"></script>
@endsection

@push('styles')
  <link href="{{asset('jquery-ui/jquery-ui.min.css')}}" rel="stylesheet" type="text/css">
  <link href="{{asset('css/novel.css')}}" rel="stylesheet" type="text/css">
@endpush