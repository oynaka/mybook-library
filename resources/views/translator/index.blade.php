@extends('layouts.master')
@section('content')
<div class="row">
    <div class="col-lg-12">
        <h3 class="page-header">Translator</h3>
    </div>
    <!-- /.col-lg-12 -->
</div>

<div class="row">
      <div class="panel panel-default">
        <div class="panel-heading clearfix">
          <form class="navbar-form navbar-left" role="search">
            {{ csrf_field() }}
            <div class="form-group">
              <input type="text" id="search_name" name="search_name" class="form-control" placeholder="Search" value="{{$search_name}}">
            </div>
            <button type="submit" class="btn btn-default">Submit</button>
            <button type="button" class="btn btn-default" id="reset_search">Reset</button>
          </form>

          <div class="navbar-form navbar-right">
            <a  href="/translator/insert" class="btn btn-info btn-sm" target="_blank">Add a new</a>
          </div>
        </div>
        <!-- panel-heading clearfix -->
      </div>
      <!-- panel panel-default -->

      <!-- Table -->
      <table class="table table-striped table-bordered table-condensed">
        <thead>
          <tr>
            <th>#</th>
            <th>Name</th>
            <th>Manga Work Count</th>
            <th>Novel Work Count</th>
            <th>CE Work Count</th>
            <th>&nbsp;</th>
          </tr>
        </thead>
        <tbody>
            @if (isset($translators))
                @foreach($translators as $translator)
                    <tr>
                        <td>{{$loop->iteration + $translators->firstItem()-1}}</td>
                        <td>{{$translator->translator_name}}</td>
                        <td>{{$translator->manga_detail_count}}</td>
                        <td>{{$translator->novel_detail_count}}</td>
                        <td>{{$translator->ce_detail_count}}</td>
                        <td>
                          <a href="/translator/edit/{{$translator->transID}}" target="_blank" class="btn btn-info btn-xs" >View & Edit</a>
                        </td>
                    </tr>
                @endforeach
            @endif
        </tbody>
      </table>
      <nav style="text-align:center">{{$translators->appends(request()->except('_token'))->links()}}</nav>
</div> <!-- row -- >
@endsection