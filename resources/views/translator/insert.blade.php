@extends('layouts.master')
@section('content')
<div class="row">
        <div class="col-lg-12">
            <h3 class="page-header">Insert Translator</h3>
        </div>
        <!-- /.col-lg-12 -->
    </div>

    <div class="row">
        <div class="panel panel-default">
            <div class="panel-heading clearfix">

                @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif

                <form action="" method="POST" >
                    {{ csrf_field() }}
                    <div class="form-group">
                        <label for="translator_name">Name</label>
                        <input type="text" class="form-control" id="translator_name" name="translator_name" value="">
                    </div>

                    <div class="form-group">
                        <label for="remark">Remark</label>
                        <textarea class="form-control" id="remark" name="remark">{{old('remark')}}</textarea>
                    </div>

                    <input type="hidden" name="mode" value="insert">
                    <button type="submit" class="btn btn-primary">Submit</button>
                    <button class="btn btn-default" type="reset">Reset</button>
                </form>
            </div>
        </div>
    </div>
@endsection