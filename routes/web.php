<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/*
Route::get('/', function () {
    return view('welcome');
});
*/

Route::match(array('GET', 'POST'), "/login", array(
    'uses' => 'LoginController@init',
    'as' => 'login'
));
Route::match(array('GET', 'POST'), "/", array(
    'uses' => 'DashboardController@init',
    'as' => 'dashboard'
));
Route::match(array('GET', 'POST'), "logout", array(
    'uses' => 'LogoutController@init',
    'as' => 'logout'
));
Route::match(array('GET', 'POST'), "dashboard", array(
    'uses' => 'DashboardController@init',
    'as' => 'dashboard'
));

Route::match(array('GET', 'POST'), "booklocation", array(
    'uses' => 'BookLocationController@init',
    'as' => 'booklocation'
));

Route::match(array('GET', 'POST'), "booklocation/view/{id}", array(
    'uses' => 'BookLocationController@view',
    'as' => 'booklocation_view'
));

Route::match(array('GET', 'POST'), "booklocation/edit/{id}", array(
    'uses' => 'BookLocationController@edit',
    'as' => 'booklocation_edit'
));

Route::match(array('GET', 'POST'), "booklocation/insert", array(
    'uses' => 'BookLocationController@insert',
    'as' => 'booklocation_insert'
));

Route::match(array('GET', 'POST'), "publish", array(
    'uses' => 'BookPublishController@init',
    'as' => 'publish'
));

Route::match(array('GET', 'POST'), "publish/view/{id}", array(
    'uses' => 'BookPublishController@view',
    'as' => 'publish_view'
));

Route::match(array('GET', 'POST'), "publish/edit/{id}", array(
    'uses' => 'BookPublishController@edit',
    'as' => 'publish_edit'
));

Route::match(array('GET', 'POST'), "publish/insert", array(
    'uses' => 'BookPublishController@insert',
    'as' => 'publish_insert'
));

Route::match(array('GET', 'POST'), "oripublish", array(
    'uses' => 'BookOriPublishController@init',
    'as' => 'oripublish'
));

Route::match(array('GET', 'POST'), "oripublish/view/{id}", array(
    'uses' => 'BookOriPublishController@view',
    'as' => 'oripublish_view'
));

Route::match(array('GET', 'POST'), "oripublish/edit/{id}", array(
    'uses' => 'BookOriPublishController@edit',
    'as' => 'oripublish_edit'
));

Route::match(array('GET', 'POST'), "oripublish/insert", array(
    'uses' => 'BookOriPublishController@insert',
    'as' => 'oripublish_insert'
));

Route::match(array('GET', 'POST'), "category", array(
    'uses' => 'CategoryController@init',
    'as' => 'category'
));

Route::match(array('GET', 'POST'), "category/edit/{id}", array(
    'uses' => 'CategoryController@edit',
    'as' => 'category_edit'
));

Route::match(array('GET', 'POST'), "category/insert", array(
    'uses' => 'CategoryController@insert',
    'as' => 'category_insert'
));

Route::match(array('GET', 'POST'), "translator", array(
    'uses' => 'TranslatorController@init',
    'as' => 'translator'
));

Route::match(array('GET', 'POST'), "translator/edit/{id}", array(
    'uses' => 'TranslatorController@edit',
    'as' => 'translator_edit'
));

Route::match(array('GET', 'POST'), "translator/insert", array(
    'uses' => 'TranslatorController@insert',
    'as' => 'translator_insert'
));

Route::match(array('GET', 'POST'), "manga", array(
    'uses' => 'MangaController@init',
    'as' => 'manga'
));

Route::match(array('GET', 'POST'), "manga/view/{id}", array(
    'uses' => 'MangaController@view',
    'as' => 'manga_view'
));

Route::match(array('GET', 'POST'), "manga/insert", array(
    'uses' => 'MangaController@insert',
    'as' => 'manga_insert'
));

Route::match(array('GET', 'POST'), "novel", array(
    'uses' => 'NovelController@init',
    'as' => 'novel'
));

Route::match(array('GET', 'POST'), "novel/view/{id}", array(
    'uses' => 'NovelController@view',
    'as' => 'novel_view'
));

Route::match(array('GET', 'POST'), "novel/insert", array(
    'uses' => 'NovelController@insert',
    'as' => 'novel_insert'
));

Route::match(array('GET', 'POST'), "author", array(
    'uses' => 'AuthorController@init',
    'as' => 'author'
));

Route::match(array('GET', 'POST'), "author/edit/{id}", array(
    'uses' => 'AuthorController@edit',
    'as' => 'author_edit'
));

Route::match(array('GET', 'POST'), "author/insert", array(
    'uses' => 'AuthorController@insert',
    'as' => 'author_insert'
));

Route::match(array('GET', 'POST'), "unread", array(
    'uses' => 'UnreadController@init',
    'as' => 'unread'
));

Route::match(array('GET', 'POST'), "unread/edit/{id}", array(
    'uses' => 'UnreadController@edit',
    'as' => 'unread_edit'
));

Route::match(array('GET', 'POST'), "newbook", array(
    'uses' => 'NewBookController@init',
    'as' => 'newbook'
));

Route::match(array('GET', 'POST'), "newbook/raw", array(
    'uses' => 'NewBookController@showRawData',
    'as' => 'newbook_raw'
));

Route::match(array('GET', 'POST'), "ajax/callAuthor", array(
    'uses' => 'AjaxController@callAuthor',
    'as' => 'ajax_callAuthor'
));

Route::match(array('GET', 'POST'), "ajax/getMangaDetail", array(
    'uses' => 'AjaxController@getMangaDetail',
    'as' => 'ajax_getMangaDetail'
));

Route::match(array('GET', 'POST'), "ajax/getMangaAuthor/{id}", array(
    'uses' => 'AjaxController@getMangaAuthor',
    'as' => 'ajax_getMangaAuthor'
));

Route::match(array('GET', 'POST'), "ajax/getNovelDetail", array(
    'uses' => 'AjaxController@getNovelDetail',
    'as' => 'ajax_getNovelDetail'
));

Route::match(array('GET', 'POST'), "ajax/getCEDetail", array(
    'uses' => 'AjaxController@getCEDetail',
    'as' => 'ajax_getCEDetail'
));

Route::match(array('GET', 'POST'), "ajax/graph/setAreaGraph", array(
    'uses' => 'AjaxController@setAreaGraph',
    'as' => 'ajax_setAreaGraph'
));

Route::match(array('GET', 'POST'), "ajax/graph/setAreaGraph/{year}", array(
    'uses' => 'AjaxController@setAreaGraph',
    'as' => 'ajax_setAreaGraph'
));

Route::match(array('GET', 'POST'), "ajax/graph/setBarGraph", array(
    'uses' => 'AjaxController@setBarGraph',
    'as' => 'ajax_setBarGraph'
));

Route::match(array('GET', 'POST'), "ajax/graph/setBarReadGraph", array(
    'uses' => 'AjaxController@setBarReadGraph',
    'as' => 'ajax_setBarReadGraph'
));

Route::match(array('GET', 'POST'), "ajax/graph/setDonutGraph", array(
    'uses' => 'AjaxController@setDonutGraph',
    'as' => 'ajax_setDonutGraph'
));

Route::match(array('GET', 'POST'), "ajax/graph/setDonutGraph/{year}", array(
    'uses' => 'AjaxController@setDonutGraph',
    'as' => 'ajax_setDonutGraph'
));

Route::match(array('GET', 'POST'), "comicessay", array(
    'uses' => 'ComicEssayController@init',
    'as' => 'comicessay'
));

Route::match(array('GET', 'POST'), "comicessay/view/{id}", array(
    'uses' => 'ComicEssayController@view',
    'as' => 'comicessay_view'
));

Route::match(array('GET', 'POST'), "comicessay/insert", array(
    'uses' => 'ComicEssayController@insert',
    'as' => 'comicessay_insert'
));

Route::get('/clear-data', function () {
    return view('cleardata');
});

/* command route */
//Clear Cache facade value:
Route::get('/clear-cache', function() {
    $exitCode = Artisan::call('cache:clear');
    return '<h1>Cache facade value cleared</h1>';
});

//Route cache:
Route::get('/route-cache', function() {
    $exitCode = Artisan::call('route:cache');
    return '<h1>Routes cached</h1>';
});

//Clear Route cache:
Route::get('/route-clear', function() {
    $exitCode = Artisan::call('route:clear');
    return '<h1>Route cache cleared</h1>';
});

//Clear View cache:
Route::get('/view-clear', function() {
    $exitCode = Artisan::call('view:clear');
    return '<h1>View cache cleared</h1>';
});

//Clear Config cache:
Route::get('/config-cache', function() {
    $exitCode = Artisan::call('config:cache');
    return '<h1>Clear Config cleared</h1>';
});

Route::get('/clear-compiled', function() {
    $exitCode = Artisan::call('clear-compiled');
    return '<h1>Clear Config cleared</h1>';
});

Route::match(array('GET', 'POST'), "ajax/getMangaRemark", array(
    'uses' => 'AjaxController@getMangaRemark',
    'as' => 'ajax_getMangaRemark'
));
